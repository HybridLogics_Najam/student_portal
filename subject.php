<? include_once("head.php"); ?>
<?

/*types
faculty=1
department =2
course=3
Assign=4
*/ 

if(@$type=='') { ?><center><b><? echo "Invalid access"; ?></b></center><? }
else {  
$ignore = array('module','action','proceed','back_url','id');
if($type=='1' && ($_SESSION['current_user_type']=="0" || $_SESSION['current_user_type']=="1"))
{   

    $module = "faculty";
	$tbl = "faculty_tbl";
	$heading = array('Faculty','Description','Departments'); 
	$col = array('title','desc','id');
    $main_heading = "Faculty";
    $sub_heading = "Faculty list";
	$condition = " status=1 "; 
	$orderCondition = " order by id desc"; 
	$records = RECORD_PER_PAGE;
	$data = $myObj->doPagination('',$tbl,$records,$condition,$orderCondition,'');  
}
else if($type=='2' && ($_SESSION['current_user_type']=="0" || $_SESSION['current_user_type']=="1"))
{   
 
if(@$fid=='') {/* ?><center><b><? die('Invalid access'); ?></b></center><?  */}
    $module = "department"; 
	$tbl = "department_tbl";
	$heading = array('Department','Description','Courses');  
	$col = array('title','desc','id');
    $main_heading = "Department";
    $sub_heading = "Department list";
	$condition = " status=1 "; 
	if(@$fid!=''){$condition.=" AND faculty_id= ".$fid; }  
	$orderCondition = " order by id desc"; 
	$records = RECORD_PER_PAGE;
	$count_tbl = "course_tbl";
	$data = $myObj->doPagination('',$tbl,$records,$condition,$orderCondition,''); 	
}
else if($type=='3')   
{    
   if(@$did=='') { /*?><center><b><? die('Invalid access'); ?></b></center><? */ } 
    $module = "course";
	$tbl = "course_tbl";
	$heading = array('Course name','Description','Course Code'); 
	if(@$_SESSION['admin']){$heading[] = 'Teachers';}
	$col = array('title','desc','code','id');
    $main_heading = "Course";
    $sub_heading = "Course list"; 
	$condition = " status=1 "; 
	if(@$did!='') { $condition .= " AND department_id= ".$did; }      
	//if(!(@$_SESSION['admin'])){$condition.=" AND created_by = ".$_SESSION['current_user_id'];} 
	$orderCondition = " order by id desc";    
	$records = RECORD_PER_PAGE;
	$count_tbl = "user_tbl";
	$data = $myObj->doPagination('',$tbl,$records,$condition,$orderCondition,'');   	
} 
else if($type=='4')    
{      
  if(@$tcid=='') {/* ?><center><b><? die('Invalid access'); ?></b></center><?  */}  
    $module = "assign";
	$module2 = "attach";   
	$tbl = "teacher_course_tbl";
	$heading = array('Teacher','Department','Course','Attachments');   
	$col = array('teacher','department','course');
    $main_heading = "Assigned Courses list";
    $sub_heading = "Assigned Courses list";    
	       
	$records = RECORD_PER_PAGE;   
	if(@$_SESSION['current_user_type']=='2' || @$_SESSION['current_user_type']=='3'){$teacher_cond = " AND (u.id = ".$_SESSION['current_user_id']." OR u.assistant_id = ".$_SESSION['current_user_id'].") ";} else{$teacher_cond="";}  
	if(@$tcid!=''){@$teacher_cond.=" AND tc.teacher_id= ".$tcid; } 
	$query = "SELECT tc.id as id,d.title as department,c.title as course,u.title as teacher
	          FROM teacher_course_tbl tc
			  LEFT JOIN course_tbl c ON tc.course_id = c.id
			  LEFT JOIN department_tbl d ON tc.department_id = d.id
			  LEFT JOIN user_tbl u ON tc.teacher_id = u.id
			  WHERE u.type=2 AND u.status=1 AND tc.status=1 AND c.status=1 AND d.status=1
			  ".@$teacher_cond."
			  GROUP BY tc.id 
			  ORDER BY tc.id DESC";    
	$data = $myObj->doPagination_query('',$query,$records,'');       	
} 
else {?><center><b><? die('Invalid access'); ?></b></center><? }  
//echo "<pre>";print_r($data);  
?>
<div class="main">   
  <div class="main-inner">
    <div class="container"> 
      <div class="row">
        
        <!-- /span6 -->
        <div class="span12">
         
         
          <div class="widget widget-table action-table"> 
            <div class="widget-header"> 
            
              <i class="icon-th-list"></i>
              <h3><?=$sub_heading;?></h3>  
              
              <?php if(@$_SESSION['current_user_type']!='3') { ?>
               <a href="javascript:;" class=""  onclick="modal_data('add','<?=$module;?>','c','','<?=base64_encode($_SERVER['REQUEST_URI']);?>');"><i class="btn-icon-only icon-plus" style="font-size: 37px; 
    float: right;
       margin: 2px 0px 0px 0px;"></i></a>    		  
    <?php } ?> 
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <?  if(sizeof($data)>0) { ?>
              <table class="table table-striped table-bordered">
                <thead>
                  <tr>
                  <th width="10">S.No</th>
				  <?
				  
				   foreach($heading as $head){?><th><?=$head;?></th><? } ?> 
                     
                    <?php if(@$_SESSION['current_user_type']!='3') { ?><th class="td-actions"> </th> <?php } ?>
                  </tr>
                </thead>
                <tbody>
                  <? 
				 
				  foreach($data as $key_data_=>$data_){ ?>   
                  <tr id="tr_<?=$key_data_+1;?>">
                  <td><?=$key_data_+1;?></td>
                    <? 
						 foreach($col as $column){ 
							 if ( !(in_array($column,$ignore)) ){
						       ?><td><?=$data_->$column;?></td><?    
						       }
						 } ?>
                     <? if(@$_SESSION['admin'] || $type==4){?>    
                    <td>
                    <? if($type==1)  { 
						  
						$count_query = "SELECT count(id) as counting FROM department_tbl WHERE status =1 AND faculty_id=".$data_->id;
						$counting = $myObj->query('',$count_query); 
						$counting = @$counting[0]->counting?@$counting[0]->counting:0;
						if($counting>0){?><a href="subject?type=2&fid=<?=$data_->id;?>"><? echo @$counting;?></a><? } else {echo @$counting;}
						 } 
						 
						 else if($type==2) {
						$count_query = "SELECT count(id) as counting FROM course_tbl WHERE status =1 AND department_id=".$data_->id;
						$counting = $myObj->query('',$count_query); 
						$counting = @$counting[0]->counting?@$counting[0]->counting:0;
						if($counting>0){?> <a href="subject?type=3&did=<?=$data_->id;?>"><? echo @$counting;?></a><? } else {echo @$counting;}
						
						
						 } else if($type==3) { 
						$count_query = "SELECT count(id) as counting FROM teacher_course_tbl WHERE status =1  AND course_id=".$data_->id." AND department_id=".$data_->department_id;
						$counting = $myObj->query('',$count_query); 
						$counting = @$counting[0]->counting?@$counting[0]->counting:0;
						if($counting>0){?> <a href="user?type=2&did=<?=$data_->department_id;?>&cid=<?=$data_->id?>"><? echo @$counting;?></a><? } else {echo @$counting;}
						 }
						 else if($type==4) {
							 ?>
							 <a href="javascript:;" onclick="modal_data('add','<?=$module2;?>','u','<?=$data_->id;?>','<?=base64_encode($_SERVER['REQUEST_URI']);?>');">Show</a>
							 <?php
							 }
						  ?>  
                    </td>
                    <? } ?>
                    <?php if(@$_SESSION['current_user_type']!='3') { ?>
                    <td class="td-actions"> 
                    <? if(@$type=='3' && !(@$_SESSION['admin'])) 
					  {  
					   if(@$_SESSION['current_user_id']==@$data_->created_by) {
					   ?>
						<a href="javascript:;" class="btn btn-small btn-success" onclick="modal_data('add','<?=$module;?>','u','<?=$data_->id;?>','<?=base64_encode($_SERVER['REQUEST_URI']);?>');"><i class="btn-icon-only icon-edit"> </i></a>   
					   <? 
					   }
					  }
					else {  ?> 
                    <a href="javascript:;" class="btn btn-small btn-success" onclick="modal_data('add','<?=$module;?>','u','<?=$data_->id;?>','<?=base64_encode($_SERVER['REQUEST_URI']);?>');"><i class="btn-icon-only icon-edit"> </i></a>  
					<? } ?> 
					
					<? if(@$_SESSION['admin']) {?>
                    <a href="javascript:;" class="btn btn-danger btn-small" onclick="var del = confirm('are you sure you want to delete'); if(del){modal_data('add','<?=$module;?>','d','<?=$data_->id;?>','<?=base64_encode($_SERVER['REQUEST_URI']);?>');$('#tr<?=$key_data_+1;?>').remove();
                     $('#tr_<?=$key_data_+1;?>').hide('slow',function(){ $('#tr_<?=$key_data_+1;?>').remove(); });  
                    }"><i class="btn-icon-only icon-remove"> </i></a></td>   
                    <? } }?>  
                  </tr> 
                <? }  ?> 
                </tbody>
              </table>
			 
            </div>
             <div class="paging" style="float:right;">
			  <? if($type!='4') {$myObj->doPagination('',$tbl,$records,$condition,$orderCondition,'1');} else{
				  $data = $myObj->doPagination_query('',$query,$records,'1');
				  }?>
			  </div>
              <? } else { ?>
					<center><b>No Data Found</b></center> 
					<? } ?> 
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
          
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->
<? } ?>

<? include_once("tail.php"); ?>


