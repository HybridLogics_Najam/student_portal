<?php include_once('config.php');
check_login();
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title><?=PROJECT_NAME;?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes">
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/bootstrap-responsive.min.css" rel="stylesheet">
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600"
        rel="stylesheet">
<link href="css/font-awesome.css" rel="stylesheet">
<link href="css/pages/signin.css" rel="stylesheet" type="text/css">
<link href="css/style.css" rel="stylesheet">
<!--<link href="js/guidely/guidely.css" rel="stylesheet">--> 
<link href="css/pages/dashboard.css" rel="stylesheet">
<link href="css/faq.css" rel="stylesheet" type="text/css">

<link href="css/custom_style.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
 
<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>
<body>

<div class="navbar navbar-fixed-top">
  <div class="navbar-inner">
    <div class="container"> 
    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"><span
                    class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span> </a>
                    <a class="brand" href="index" style="margin: -1.5% 0 0 -9.5%;"><img src="img/site_logo.png" style="width:50px;"><?=PROJECT_NAME;?></a>
                    					
      <div class="nav-collapse">
        <ul class="nav pull-right">
          <!--<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i
                            class="icon-cog"></i> Account <b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li><a href="javascript:;">Settings</a></li>
              <li><a href="javascript:;">Help</a></li>
            </ul>
          </li> --> 
          <? if(@$_SESSION['current_user_id']!='') { ?>
          <li class="dropdown"><a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"><i
                            class="icon-user"></i> <? 
								$user_data = $myObj->getData('','user_tbl',array('email'),"id=".$_SESSION['current_user_id']);
								if(sizeof($user_data)>0){echo $user_data[0]->email;}
								else{
									 unset($_SESSION['current_user_id']);  session_destroy(); ?><script>location.reload();</script><? 
									 }
								?><b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li><a href="javascript:;" onclick="modal_data('add','profile','u','<?=$_SESSION['current_user_id'];?>','<?=base64_encode($_SERVER['REQUEST_URI']);?>');">My Profile</a></li> 
              <li><a href="javascript:;" onClick="logout()">Logout</a></li> 
            </ul>
          </li>
          <? }
		  else { ?>
			        <li><a href="login">
							<i class="icon-signin"></i>
							Login
						</a></li>
                    <li><a href="signup">
							<i class="icon-signin"></i>
							Register
						</a></li>   
			  <? }
		  
		   ?>
        </ul>
        <!--<form class="navbar-search pull-right">
          <input type="text" class="search-query" placeholder="Search">
        </form> -->
      </div>
      <!--/.nav-collapse --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /navbar-inner --> 
</div>

<? if(@$_SESSION['current_user_id']!='')
	  {
 include_once("menu.php"); 
	  } else {echo "<br/>";}
?>
<? if(@$_SESSION['msg'] || @$_SESSION['msg_arr']) { ?> 
<div class="">   
  <div class="main-inner">
    <div class="container"> 
      <div class="row">
        <div class="span12">
<? 
		 if(@$_SESSION['msg']){ ?>
		 <div class="controls">
          <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <strong>Success!</strong> <? echo $_SESSION['msg'];unset($_SESSION['msg']); ?>
          </div>
         </div> 

		 <? }
		 if(@$_SESSION['msg_arr']){
			 foreach($_SESSION['msg_arr'] as $m_a){
				 $mes = explode("_-_",$m_a); ?>
                 <div class="controls">
          <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <strong>Warning!</strong> <? echo $mes[0]." <i>".$mes[2]."</i>";?>
          </div>
         </div>
				 <? 
				 }
			 unset($_SESSION['msg_arr']);	 
			 }
		 ?>
         </div>
        </div>
       </div>
      </div>
     </div>
     <? } ?>
