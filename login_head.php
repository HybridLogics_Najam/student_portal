<?php 
include_once('config.php');
check_login();
 ?> 
<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title><?=PROJECT_NAME;?></title>

	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes"> 
    
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css" />

<link href="css/font-awesome.css" rel="stylesheet">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    
<link href="css/style.css" rel="stylesheet" type="text/css">
<link href="css/pages/signin.css" rel="stylesheet" type="text/css">
<link href="css/custom_style.css" rel="stylesheet" type="text/css"> 

</head>

<body>
	<div id="fadeAll" class="modal fade" style="background-color: rgba(255, 255, 255, 0.79)"><center><div id='loaderInnerDiv'><img id='loaderImage' src='img/loader.gif' alt='Loading... .'/></div></center>
    </div>
	<div class="navbar navbar-fixed-top">
	
	<div class="navbar-inner">
		
		<div class="container"> 
			
			<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</a>
			
			 <a class="brand" href="index" style="margin: -1.5% 0 0 -9.5%;"><img src="img/site_logo.png" style="width:50px;"><?=PROJECT_NAME;?></a>
                    					
			
			<div class="nav-collapse">
				<ul class="nav pull-right">
					
					<!--<li class="">						
						<a href="javascript:;" class="">
							Don't have an account?
						</a>
						
					</li> -->
					
					<li class="">						
						<a href="index" class="">
							<i class="icon-chevron-left"></i>
							Back to Homepage
						</a>
						
					</li>
				</ul>
				
			</div><!--/.nav-collapse -->	
	
		</div> <!-- /container -->
		
	</div> <!-- /navbar-inner -->
	
</div> <!-- /navbar -->
<? if(@$_SESSION['msg'] || @$_SESSION['msg_arr']) { ?>
<div class="">   
  <div class="main-inner">
    <div class="container"> 
      <div class="row">
        <div class="span12">
<? 
		 if(@$_SESSION['msg']){ ?>
		 <div class="controls">
          <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <strong>Success!</strong> <? echo $_SESSION['msg'];unset($_SESSION['msg']); ?>
          </div>
         </div> 

		 <? }
		 if(@$_SESSION['msg_arr']){
			 foreach($_SESSION['msg_arr'] as $m_a){
				 $mes = explode("_-_",$m_a); ?>
                 <div class="controls">
          <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <strong>Warning!</strong> <? echo $mes[0]." <i>".$mes[2]."</i>";?>
          </div>
         </div>
				 <? 
				 }
			 unset($_SESSION['msg_arr']);	 
			 }
		 ?>
         </div>
        </div>
       </div>
      </div>
     </div>
     <? } ?>