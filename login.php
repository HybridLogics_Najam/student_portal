<? include_once("login_head.php"); ?>


<div class="account-container">
	<div class="panel-body" id="panel-body">
	<div class="content clearfix">
		
		<form method="post" id="login-form" name="login-form">
		<div id="loginMsg" style="display:none;"> </div>
			<h1>Portal Login</h1>		
			
			<div class="login-fields">
				
				<p>Please provide your details</p>
				
				<div class="field">
					<label for="email">Email</label>
					<input type="email" id="email" name="email" value="" placeholder="Email" class="login username-field" autocomplete="off" required/>
                    
				</div> <!-- /field -->
				
				<div class="field">
					<label for="password">Password:</label>
					<input type="password" id="password" name="password" value="" placeholder="Password" class="login password-field" autocomplete="off" required/> 
				</div> <!-- /password -->
				
			</div> <!-- /login-fields -->
			
			<div class="login-actions"> 
				
				<!--<span class="login-checkbox">
					<input id="Field" name="Field" type="checkbox" class="field login-checkbox" value="First Choice" tabindex="4" />
					<label class="choice" for="Field">Keep me signed in</label>
				</span> -->
					 				
				<input type="button" class="button btn btn-success btn-large" value="Sign In" onClick="login('<?php if(isset($last_path)){echo $last_path;} else {echo base64_encode("index".SECURE);}?>')"> 
				
			</div> <!-- .actions -->
			
			
			
		</form>
		
	</div> <!-- /content -->
	</div>
</div> <!-- /account-container -->



<!--<div class="login-extra">
	<a href="#">Reset Password</a>
</div> -->

<? include_once("login_tail.php"); ?>