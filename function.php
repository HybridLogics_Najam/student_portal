<?


function total_pagess($query,$records_per_page){
		//echo $query;
		$query_result=mysql_query($query);
		$number_of_records=mysql_num_rows($query_result);
		$total_pages=ceil($number_of_records/$records_per_page);
		return $total_pages;
	}
	
	 function week_dates($year, $week)
{
 
	for($i=1;$i<=7;$i++) {
		$dates[] = date("Y-m-d", strtotime("{$year}-W{$week}-$i"));
		
}

return $dates;

//return “Week {$week} in {$year} is from {$from} to {$to}.”;
} 

function week_dates_and_days($year, $week)
{
$monday = date("Y-m-d", strtotime("{$year}-W{$week}-1"));
$tuesday = date("Y-m-d", strtotime("{$year}-W{$week}-2"));
$wednesday = date("Y-m-d", strtotime("{$year}-W{$week}-3"));
$thursday = date("Y-m-d", strtotime("{$year}-W{$week}-4"));
$friday = date("Y-m-d", strtotime("{$year}-W{$week}-5"));
$saturday = date("Y-m-d", strtotime("{$year}-W{$week}-6"));
$sunday = date("Y-m-d", strtotime("{$year}-W{$week}-7"));
	
	return $dates = array("monday"=>$monday,"tuesday"=>$tuesday,"wednesday"=>$wednesday,"thursday"=>$thursday,"friday"=>$friday,"saturday"=>$saturday,"sunday"=>$sunday,);
}

 function first_date_of_month($month,$year)  
 {  
      return date("Y/m/d", strtotime($month.'/01/'.$year.' 00:00:00'));  
 }  
 function last_date_of_month($month,$year)  
 {  
      return date("Y/m/d", strtotime('-1 second',strtotime('+1 month',strtotime($month.'/01/'.$year.' 00:00:00'))));  
 }
 
 function total_days_of_month($month,$year) 
{ 
// calculate number of days in a month 
return $month == 2 ? ($year % 4 ? 28 : ($year % 100 ? 29 : ($year % 400 ? 28 : 29))) : (($month - 1) % 7 % 2 ? 30 : 31); 
}
 
 function generate_calendar($year, $month, $days = array(), $day_name_length = 3, $month_href = NULL, $first_day = 0, $pn = array()){
    $first_of_month = gmmktime(0,0,0,$month,1,$year);

    #remember that mktime will automatically correct if invalid dates are entered
    # for instance, mktime(0,0,0,12,32,1997) will be the date for Jan 1, 1998
    # this provides a built in "rounding" feature to generate_calendar()

    $day_names = array(); #generate all the day names according to the current locale
    for($n=0,$t=(3+$first_day)*86400; $n<7; $n++,$t+=86400) #January 4, 1970 was a Sunday
        $day_names[$n] = ucfirst(gmstrftime('%A',$t)); #%A means full textual day name

    list($month, $year, $month_name, $weekday) = explode(',',gmstrftime('%m,%Y,%B,%w',$first_of_month));
    $weekday = ($weekday + 7 - $first_day) % 7; #adjust for $first_day
    $title   = htmlentities(ucfirst($month_name)).'&nbsp;'.$year;  #note that some locales don't capitalize month and day names

    #Begin calendar. Uses a real <caption>. See http://diveintomark.org/archives/2002/07/03
    @list($p, $pl) = each($pn); @list($n, $nl) = each($pn); #previous and next links, if applicable
    if($p) $p = '<span class="calendar-prev">'.($pl ? '<a href="'.htmlspecialchars($pl).'">'.$p.'</a>' : $p).'</span>&nbsp;';
    if($n) $n = '&nbsp;<span class="calendar-next">'.($nl ? '<a href="'.htmlspecialchars($nl).'">'.$n.'</a>' : $n).'</span>';
    $calendar = '<table class="calendar">'."\n".
        '<caption class="calendar-month">'.$p.($month_href ? '<a href="'.htmlspecialchars($month_href).'">'.$title.'</a>' : $title).$n."</caption>\n<tr>";

    if($day_name_length){ #if the day names should be shown ($day_name_length > 0)
        #if day_name_length is >3, the full name of the day will be printed
        foreach($day_names as $d)
            $calendar .= '<th abbr="'.htmlentities($d).'">'.htmlentities($day_name_length < 4 ? substr($d,0,$day_name_length) : $d).'</th>';
        $calendar .= "</tr>\n<tr>";
    }

    if($weekday > 0) $calendar .= '<td colspan="'.$weekday.'">&nbsp;</td>'; #initial 'empty' days
    for($day=1,$days_in_month=gmdate('t',$first_of_month); $day<=$days_in_month; $day++,$weekday++){
        if($weekday == 7){
            $weekday   = 0; #start a new week
            $calendar .= "</tr>\n<tr>";
        }
        if(isset($days[$day]) and is_array($days[$day])){
            @list($link, $classes, $content) = $days[$day];
            if(is_null($content))  $content  = $day;
            $calendar .= '<td'.($classes ? ' class="'.htmlspecialchars($classes).'">' : '>').
                ($link ? '<a href="'.htmlspecialchars($link).'">'.$content.'</a>' : $content).'</td>';
        }
        else $calendar .= "<td>$day</td>";
    }
    if($weekday != 7) $calendar .= '<td colspan="'.(7-$weekday).'">&nbsp;</td>'; #remaining "empty" days

    return $calendar."</tr>\n</table>\n";
}
//echo generate_calendar(2014, 12, 16,3,NULL,0,15, $first_of_month, $day_names, $day_names[$n]);
#echo generate_calendar($year, $month, $days,$day_name_length,$month_href,$first_day,$pn);
function day_of_date($date) {
return date('D', strtotime($date));
}

 function year_month_day_of_date($date) {
return array("year"=>date('Y', strtotime($date)),"month"=>date('m', strtotime($date)),"day"=>date('D', strtotime($date)));
}

 function all_dates_of_month($date,$ignoreDaysArray)  
 {  
  $finalDate = explode("-",$date);

// calculate number of days in a month 
 $totalDays =  $finalDate[1] == 2 ? ($finalDate[0] % 4 ? 28 : ($finalDate[0] % 100 ? 29 : ($finalDate[0] % 400 ? 28 : 29))) : (($finalDate[1] - 1) % 7 % 2 ? 30 : 31); 
 for($i=1;$i<=$totalDays;$i++)
 {
	 
	 if( !(in_array(date('D', strtotime($finalDate[1].'/'.$i.'/'.$finalDate[0])),$ignoreDaysArray)) ) // It will filter the days which you want
	 {
	 $arr[] = array(date("Y-m-d", strtotime($finalDate[1].'/'.$i.'/'.$finalDate[0].' 00:00:00')),"day"=>date('D', strtotime($finalDate[1].'/'.$i.'/'.$finalDate[0])));
	 }
	}
	return $arr; 
      
 }

 function all_dates_of_previous_month($date,$ignoreDaysArray)  
 {  
  $finalDate = explode("-",$date);
  $finalDate[1] = $finalDate[1]-1;
// calculate number of days in a month 
 $totalDays =  $finalDate[1] == 2 ? ($finalDate[0] % 4 ? 28 : ($finalDate[0] % 100 ? 29 : ($finalDate[0] % 400 ? 28 : 29))) : (($finalDate[1] - 1) % 7 % 2 ? 30 : 31); 
 for($i=1;$i<=$totalDays;$i++)
 {
	 
	 if( !(in_array(date('D', strtotime($finalDate[1].'/'.$i.'/'.$finalDate[0])),$ignoreDaysArray)) ) // It will filter the days which you want
	 {
	 $arr[] = array(date("Y-m-d", strtotime($finalDate[1].'/'.$i.'/'.$finalDate[0].' 00:00:00')),"day"=>date('D', strtotime($finalDate[1].'/'.$i.'/'.$finalDate[0])));
	 }
	}
	return $arr; 
      
 }
 function all_dates_of_next_month($date,$ignoreDaysArray)  
 {  
  $finalDate = explode("-",$date);
  $finalDate[1] = $finalDate[1]+1;
// calculate number of days in a month 
 $totalDays =  $finalDate[1] == 2 ? ($finalDate[0] % 4 ? 28 : ($finalDate[0] % 100 ? 29 : ($finalDate[0] % 400 ? 28 : 29))) : (($finalDate[1] - 1) % 7 % 2 ? 30 : 31); 
 for($i=1;$i<=$totalDays;$i++)
 {
	 
	 if( !(in_array(date('D', strtotime($finalDate[1].'/'.$i.'/'.$finalDate[0])),$ignoreDaysArray)) ) // It will filter the days which you want
	 {
	 $arr[] = array(date("Y-m-d", strtotime($finalDate[1].'/'.$i.'/'.$finalDate[0].' 00:00:00')),"day"=>date('D', strtotime($finalDate[1].'/'.$i.'/'.$finalDate[0])));
	 }
	}
	return $arr; 
      
 }
 
 function my_month($date)
 {
   $date_arr=explode('-',$date);
	 for($i=0;$i<30;$i++) 
	 {
		 
         $my_days[$i+1]= array("date"=>date("Y-m-d",mktime(0,0,0,$date_arr[1],$date_arr[2]+$i,$date_arr[0])),"day"=>date('D',mktime(0,0,0,$date_arr[1],$date_arr[2]+$i,$date_arr[0])));
	 }
	 return $my_days;
	}  
	 
function get_month_name_using_month_number($month_number)
{
return $monthName = date('F', mktime(0, 0, 0, $month_number, 10));	
}	 
function get_formated_time($dumpTime)
{
	return date('h:i A', strtotime($dumpTime));
}

function get_ip_address()
{
$ipaddress = '';
    if (@$_SERVER['HTTP_CLIENT_IP'])
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    else if(@$_SERVER['HTTP_X_FORWARDED_FOR'])
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    else if(@$_SERVER['HTTP_X_FORWARDED'])
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    else if(@$_SERVER['HTTP_FORWARDED_FOR'])
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    else if(@$_SERVER['HTTP_FORWARDED'])
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
    else if(@$_SERVER['REMOTE_ADDR'])
        $ipaddress = $_SERVER['REMOTE_ADDR'];
    else
        $ipaddress = 'Unknown';

    return $ipaddress;
}
function get_location_using_ip()
{
	$ipaddress = '';
    if (@$_SERVER['HTTP_CLIENT_IP'])
        $ipaddress = $_SERVER['HTTP_CLIENT_IP']; 
    else if(@$_SERVER['HTTP_X_FORWARDED_FOR'])
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    else if(@$_SERVER['HTTP_X_FORWARDED'])
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    else if(@$_SERVER['HTTP_FORWARDED_FOR'])
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    else if(@$_SERVER['HTTP_FORWARDED'])
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
    else if(@$_SERVER['REMOTE_ADDR'])
        $ipaddress = $_SERVER['REMOTE_ADDR'];
    else
        $ipaddress = 'Unknown';

    $ip_addr = $ipaddress;
	
 /*$client_ip  = @$_SERVER['HTTP_CLIENT_IP'];
 $forward_ip = @$_SERVER['HTTP_X_FORWARDED_FOR'];
 $remote_ip  = @$_SERVER['REMOTE_ADDR'];*/
 $return_data  = array('country'=>'', 'city'=>'');
 /*if(filter_var($client_ip, FILTER_VALIDATE_IP))
 {
  //$ip_addr = $client_ip;
 }
 elseif(filter_var($forward_ip, FILTER_VALIDATE_IP))
 {
  //$ip_addr = $forward_ip;
 }
 else
 {
  //$ip_addr = $remote_ip;
 }*/
 $ip_data = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=".$ip_addr));
 if($ip_data && $ip_data->geoplugin_countryName != null)
 {
  $return_data['country'] = $ip_data->geoplugin_countryCode;
  $return_data['city'] = $ip_data->geoplugin_city;
 }
 return $return_data;
}
function get_mac_address()
{
	$mac = "";
// Turn on output buffering
ob_start();
//Get the ipconfig details using system commond
system('ipconfig /all');

// Capture the output into a variable
$mycom=ob_get_contents();
// Clean (erase) the output buffer
ob_clean();

$findme = "Physical";
//Search the "Physical" | Find the position of Physical text
$pmac = strpos($mycom, $findme);

// Get Physical Address
$mac=substr($mycom,($pmac+36),17);
//Display Mac Address
return $mac;	
}
function get_exact_location_using_ip($ipAddr='') // This is short method of ipinfodb to get exact location
{
    $url = "http://api.ipinfodb.com/v3/ip-city/?key=5cfaab6c5af420b7b0f88d289571b990763e37b66761b2f053246f9db07ca913&ip=$ipAddr&format=json";
    $d = @file_get_contents($url);
	return $d;
    //return json_decode($d , true);
}

function addressToLatLng($test='',$address)
{
 $coords = array();
    /*
      OBSOLETE, now using utf8_encode
      // replace special characters (eg. German "Umlaute")
      $address = str_replace("ä", "ae", $address);
      $address = str_replace("ö", "oe", $address);
      $address = str_replace("ü", "ue", $address);
      $address = str_replace("Ä", "Ae", $address);
      $address = str_replace("Ö", "Oe", $address);
      $address = str_replace("Ü", "Ue", $address);
      $address = str_replace("ß", "ss", $address);
    */
    $address = utf8_encode($address);
    // call geoencoding api with param json for output
    $geoCodeURL = "http://maps.google.com/maps/api/geocode/json?address=".urlencode($address)."&sensor=false"; 
    $result = json_decode(file_get_contents($geoCodeURL), true);
	if($test != '' || $test != NULL)
	{return $result;} 
    $coords['status'] = $result["status"];
    $coords['lat'] = $result["results"][0]["geometry"]["location"]["lat"];
    $coords['lng'] = $result["results"][0]["geometry"]["location"]["lng"];
    return $coords;
}
function latLngToAddress($lat='',$lng='')
{
// call geoencoding api with param json for output
$geoCodeURL = "https://maps.googleapis.com/maps/api/geocode/json?latlng=".$lat.",".$lng."&location_type=ROOFTOP&result_type=street_address&key=AIzaSyAG3cqsVPD9plu5PqtyElL9s7a8W1yhpRM"; 
 $result = json_decode(file_get_contents($geoCodeURL), true);
 return $result;
}
?>
<?php 
//Upload file & image functions
function compressImage($ext,$uploadedfile,$path,$actual_image_name,$newwidth)
{
//echo $uploadedfile." PP";die();
if($ext=="jpg" || $ext=="jpeg" || $ext=="JPG" || $ext=="JPEG")
{
$src = imagecreatefromjpeg($uploadedfile);
}
else if($ext=="png" || $ext=="PNG")
{
$src = imagecreatefrompng($uploadedfile);
}
else if($ext=="gif" || $ext=="GIF")
{
$src = imagecreatefromgif($uploadedfile);
}
else
{
$src = imagecreatefrombmp($uploadedfile);
}

list($width,$height)=getimagesize($uploadedfile);
$newheight=($height/$width)*$newwidth;
$tmp=imagecreatetruecolor($newwidth,$newheight);
imagecopyresampled($tmp,$src,0,0,0,0,$newwidth,$newheight,$width,$height);
$filename = $path.$newwidth.'_'.$actual_image_name; //PixelSize_TimeStamp.jpg
imagejpeg($tmp,$filename,100);
imagedestroy($tmp);
return $filename;
}
function getExtension($str)
{
$i = strrpos($str,".");
if (!$i)
{
return "";
}
$l = strlen($str) - $i;
$ext = substr($str,$i+1,$l);
return $ext;
}
/*function upload_file($file_name='',$file_temp_name='',$file_size='',$upload_path='',$allowed_extension='',$allowed_size='',$type='',$widthArray='') {
	$file_ = $file;
if($upload_path=='') {$path = "uploads/"; } else {$path = $upload_path;}

// check directory existance
if( !(is_dir($upload_path)) )
  { echo "Invalid Directory ".$upload_path;exit; }
// /check directory existance

if($type=='image')
{

if($allowed_extension=='') {$allowed_extension = array("jpg", "png", "gif", "bmp","jpeg","PNG","JPG","JPEG","GIF","BMP"); }
if($allowed_size=='') {$allowed_size=1024*1024*1;} else {$allowed_size=1024*1024*$allowed_size;}

if(isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST")
{

$imagename = $file_['name'];
$size = $file_['size'];
if(strlen($imagename))
{
$ext = strtolower(getExtension($imagename));
if(in_array($ext,$allowed_extension))
{
if($size<($allowed_size)) // Image size max 1 MB default size is 1 mb
{
$actual_image_name = "_".time().".".$ext;
$uploadedfile = $file_['tmp_name'];

//if($widthArray=='') { $widthArray = array(200,100,50); } //You can change dimension here.
if($widthArray!='')
{
foreach($widthArray as $newwidth)
{
$filename=compressImage($ext,$uploadedfile,$path,$actual_image_name,$newwidth);
//echo "<img src='".$filename."' class='img'/>";
}
}
//Original Image
if(move_uploaded_file($uploadedfile, $path.$actual_image_name))
{
//Insert upload image files names into user_uploads table
//mysqli_query($db,"UPDATE users SET profile_image='$actual_image_name' WHERE uid='$session_id';");
//echo "<img src='uploads/".$actual_image_name."' class='preview'>";


return $actual_image_name;
}
else
echo "failed";
}
else
echo "Image file size max ".$allowed_size." MB"; 
}
else
echo "Invalid file format.."; 
}
else
echo "Please select image..!";
exit;
} }
}*/
//Upload file & image functions
 
function validateField($fieldValue,$fieldType='',$required='',$minLength='',$maxLength='') // Msgs (true for success,0 for required, 2 for invalid email, 3 for minimum Length, 4 for maximum Length)
{  

  $arr_err = array();
  $emailPatternCheck=true;
  if($fieldType=='') {$fieldType = "Field"; }
	
		if($fieldType=='email')
	    { $emailPatternCheck = (filter_var($fieldValue, FILTER_VALIDATE_EMAIL)); }
	
		if($required=='1')
		{
			 
	     if(ucfirst($fieldType)=='Email'){if($emailPatternCheck==false){ $arr_err = array($fieldType,'2',"Invalid ".$fieldType); return $arr_err;} }
		 if($minLength!='') {if(strlen($fieldValue)<$minLength) {$arr_err = array($fieldType,'3',$fieldType." must be atleast ".$minLength." characters"); return $arr_err;}}
		 if($maxLength!='') {if(strlen($fieldValue)>$maxLength) {$arr_err = array($fieldType,'4',$fieldType." must be less than ".$minLength." characters"); return $arr_err;}}
		 if($fieldValue!='') {return "1";}
		 else
		    {
			 if($fieldValue=='') { $arr_err = array($fieldType,'0',$fieldType." cant be Empty "); return $arr_err;} 
			}
		}
	   else
		{
			if($fieldType=='email'){if($emailPatternCheck==false){ $arr_err = array($fieldType,'2',"Invalid ".$fieldType); return $arr_err; } }
			else {return "1"; }
	    }
	
}

function check_login(){
	
	if(@$_SESSION['current_user_id']!='' && is_numeric(@$_SESSION['current_user_id']) && basename($_SERVER['PHP_SELF'])=='login.php') { ?>
<script>window.location.href="index";</script> 
<? } 
else if(!(@$_SESSION['current_user_id']) && (basename($_SERVER['PHP_SELF'])!='login.php' && basename($_SERVER['PHP_SELF'])!='signup.php' && basename($_SERVER['PHP_SELF'])!='index.php')){ ?>
<script>window.location.href="login?last_path=<?php echo base64_encode(ADMIN_CURRENT_PATH_SERVER.SECURE);?>";</script>
	<? }
	}  

?>
