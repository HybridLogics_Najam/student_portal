// JavaScript Document
function showLoader(div)
{
	
var loader_body = "<center><div id='loaderInnerDiv'><img id='loaderImage' src='img/loader.gif' alt='Loading... .'/></div></center>";	
$("#"+div).prepend("<div id='loaderDiv' class='loadingClass'> </div>");

$("#loaderDiv").height($('#'+div).height());
$("#loaderDiv").width($('#'+div).width());
$("#loaderDiv").css({"max-height":$(window).height(),"max-width":$(document).width()})

$("#loaderDiv").html(loader_body);
$("#loaderDiv div img").css("margin-top",$('#'+div).height()-($('#'+div).height()/2));
$("#top_right_profile_box").show();
}
function hideLoader(div) 
{
	setTimeout(function() { $("#"+div+" > #loaderDiv").hide(); }, 500);
	
}

function showMessage(type,title,msg)
{
var msg = "<div class='alert alert-"+type+"' role='alert' style=''><button type='button' class='close' data-dismiss='alert'>&times;</button><strong style='float:left;margin-right: 3%;'>"+title+"!</strong><div id='alertMsg'>"+msg+".</div></div>";	
$("#loginMsg").html(msg);
$("#loginMsg").css({"display":"block"});	 
}

function base64_decode(data) {
  //  discuss at: http://phpjs.org/functions/base64_decode/
  // original by: Tyler Akins (http://rumkin.com)
  // improved by: Thunder.m
  // improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  //    input by: Aman Gupta
  //    input by: Brett Zamir (http://brett-zamir.me)
  // bugfixed by: Onno Marsman
  // bugfixed by: Pellentesque Malesuada
  // bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  //   example 1: base64_decode('S2V2aW4gdmFuIFpvbm5ldmVsZA==');
  //   returns 1: 'Kevin van Zonneveld'
  //   example 2: base64_decode('YQ===');
  //   returns 2: 'a'

  var b64 = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';
  var o1, o2, o3, h1, h2, h3, h4, bits, i = 0,
    ac = 0,
    dec = '',
    tmp_arr = [];

  if (!data) {
    return data;
  }

  data += '';

  do { // unpack four hexets into three octets using index points in b64
    h1 = b64.indexOf(data.charAt(i++));
    h2 = b64.indexOf(data.charAt(i++));
    h3 = b64.indexOf(data.charAt(i++));
    h4 = b64.indexOf(data.charAt(i++));

    bits = h1 << 18 | h2 << 12 | h3 << 6 | h4;

    o1 = bits >> 16 & 0xff;
    o2 = bits >> 8 & 0xff;
    o3 = bits & 0xff;

    if (h3 == 64) {
      tmp_arr[ac++] = String.fromCharCode(o1);
    } else if (h4 == 64) {
      tmp_arr[ac++] = String.fromCharCode(o1, o2);
    } else {
      tmp_arr[ac++] = String.fromCharCode(o1, o2, o3);
    }
  } while (i < data.length);

  dec = tmp_arr.join('');

  return dec.replace(/\0+$/, '');
}

function validateField(FieldID,FieldType,required,length) // Msgs (true for success,0 for required, 2 for invalid email, 3 for length)
{
	$("#"+FieldID).removeClass("hasError");
	var fieldValue = $("#"+FieldID).val();
	if(FieldType=='email')
	{
		
	var emailPattern = new RegExp(/^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/);
    var emailPatternCheck = emailPattern.test(fieldValue);
	
  if(required=='1'){ 
  if(length!='') {if(fieldValue.length<length) {return "3";}}
   if(fieldValue=='') {return "0";}}
			  
			  
	if(emailPatternCheck==true) { return true; }
	else if(emailPatternCheck==false){ return "2"; }	
	}
	else //if(FieldType=='password')
	{
		if(required=='1')
		{
		if(length!='') {if(fieldValue.length<length) {return "3";}}
		if(fieldValue!='') {return true;}
		else
		{
			if(fieldValue=='') { return "0";} 
			}
		}
		else
		{return true;}
	    
		
	}
	
	
  
}
function login(last_url)
{
	 
 showLoader('panel-body'); 
 var data = $("#login-form").serialize();
//console.log(data);  
   var validatePasswordResponce = validateField('password','password','1','');
  var validateEmailResponce = validateField('email','email','1','');
  if(validateEmailResponce== "0") {$("#email").addClass("hasError");hideLoader('panel-body');
			  setTimeout(function(){showMessage('danger','Failed','Email can\'t be empty');},500);$("#password").val('');return false;}
  if(validateEmailResponce== "2") {$("#email").addClass("hasError");hideLoader('panel-body');
			  setTimeout(function(){showMessage('danger','Failed','Invalid Email');},500);$("#password").val('');return false;}
  if(validatePasswordResponce== "0") {$("#password").addClass("hasError");hideLoader('panel-body');
			  setTimeout(function(){showMessage('danger','Failed','Password can\'t be empty');},500);return false;}
 if(validateEmailResponce == true && validatePasswordResponce == true) 
  {
 $.ajax({
          url:'login_action',
		  type : 'POST',
		  data: data,
          success:function(myData){ 
			  //console.log(myData);
			  myData = $.trim(myData); 
			  if(myData==1 || myData=='1')
			  {hideLoader('panel-body');
			  setTimeout(function(){showMessage('success','Success','You are being redirect ...');},500);
			  setTimeout(function(){
				  
				  var final_url = base64_decode(last_url).split('_-_');
                  //console.log(final_url);
				
				  window.location.href= final_url[0];
				  
				  },2000);

			  }
			  else if(myData==0){hideLoader('panel-body');
			  setTimeout(function(){showMessage('danger','Failed','Invalid Email or Password');},500);
			  
			
			  $("#password").addClass("hasError");
			  }
			   else if(myData==2){hideLoader('panel-body');
			  setTimeout(function(){showMessage('danger','Failed','Your account approval is pending');},500);
			  
			  
			  }
			  
			  else if(myData==3){hideLoader('panel-body');
			  setTimeout(function(){showMessage('danger','Failed','Your account is Deactive');},500);
			  
			  }
			
		  
			
          }
        });
}
else
 {

 }
 $("#email").css({"border-bottom":"0px"});
 
}
function showLoader2() {
	
	$("#fadeAll").modal('show');
	$("#fadeAll div img").css("margin-top","20%");
	$("#fadeAll div img").css("margin-bottom","20%");
	$("#fadeAll").css("opacity","0.70"); 
	
	
	}
function hideLoader2()
	{
		$("#fadeAll").modal('hide');
		//setTimeout(function(){$("#fadeAll").modal('hide');},1000);
		
		
		
	}
function logout()
{
	//showLoader2(); 
	$.ajax({
          url:'logout_action',
		  type : 'POST',
          success:function(data){
		  data = $.trim(data);	  
			  if(data==1) 
			  {
			 
			 //setTimeout(function(){hideLoader2();window.location.href="login";},1500);
             window.location.href="login";
			  }
			  else { }
			
		  
			
          }
        });
}

function get_display_data(form,file,div,page_id,order_by,order)
{
	showLoader2();
 var data = "page="+page_id+"&order_by="+order_by +"&order="+order +"&" + $('#'+form).serialize();
 $.ajax({
          url:file+'.php',
		  type: "POST",
		  data: data,
          success:function(returnData){
			  hideLoader2();
			  //setTimeout(function(){ $("#"+div).html(returnData); },2500);
             $("#"+div).html(returnData);
          }
		 });
		 
		 
}


function show_modal(){
		$("#myModal").modal('show');
}

function modal_data(file,module,action,id,back_url){
	//console.log(action);
	var data = "module="+module+"&action="+action+"&id="+id+"&back_url="+back_url; 
	if(action=='d' || action=='ap' || action=='uu'){
		 $.ajax({
          url:'action',
		  type: "POST",
		  data: data,
          success:function(returnData){
			 returnData = $.trim(returnData);
			 console.log(returnData);
			 if(action=='ap') {location.reload();}
          }
		 });  
		} 
    else{
	show_modal(); 
	 $.ajax({
          url:file,
		  type: "POST",
		  data: data,
          success:function(returnData){
			  //console.log();
			 $("#myModal").html(returnData);  
          }
		 });   
	} 
}

function action(module,action,myform){

	var data = "module="+module+"&action="+action+"&"+$("#"+myform).serialize();  
	 $.ajax({
          url:'action',
		  type: "POST",
		  data: data,
          success:function(returnData){
			  returnData = $.trim(returnData);
			  //console.log(returnData);
			  if(action=='c' || action=='u' || action=='d') 
			  {
				$('#myModal').modal('hide');
			    //alert(returnData);  
				window.location.reload();
			  } 
			  else 
			  {
				  
			  } 
			    
          }  
		 });     
	
}

function validate_user(){
var err_html= "<label style='color:#F00; font-size: 12px;'>Confirm Password doesn\'t match with Password !<label> ";
var id = $("#id").val()?$("#id").val():'';

    uniqueness('teacher','email','c',id);
	
	var password = $("#password").val();
	var confirm_password = $("#confirm_password").val();
	if(confirm_password!=password){
		        $("#confirm_password").val('').focus();
	            $("#confirm_password").next().remove();
				$("#confirm_password").after(err_html);
				 
	 return false;}
	else{ 
	    $("#confirm_password").next().remove();
	     if($('#error_').val()==''){
	           return true;} 
		 else {return false;}  
	}
	 
} 
	
function uniqueness(module,elementid,action,id){

var val = $("#"+elementid).val();
var err_html= "<label style='color:#F00; font-size: 12px;'>"+val+" Already exists !<label> ";
	//console.log(id);
	 $.ajax({ 
          url:'ajax_action',
		  type: "POST",
		  data: { module : module, elementid: elementid,val:val, id: id, action:action },
		  async:false,
          success:function(returnData){
			  returnData = $.trim(returnData);
			  //console.log(returnData);
			  if(returnData.trim()=='1' || returnData.trim()==1)
			  {
			    $('#'+elementid).next().remove();
				$('#'+elementid).after(err_html);
				$('#error_').val('1'); 
				$('#'+elementid).focus();
				//$('#'+elementid).val("");
			  }
			  
			  else {
				   if(module=='assign') {
				     $('#course_id').html(returnData);
			       }
				   else { 
				  $('#'+elementid).next().remove();
				  $('#error_').val(''); 
				   }
				  }   
			    
          }  
		 });      
	
}

function uniqueness2(module,elementid,action,id){

var val = $("#"+elementid).val();

var err_html= "<label style='color:#F00; font-size: 12px;'>"+val+" Already exists !<label> ";
	
	 $.ajax({ 
          url:'ajax_action',
		  type: "POST",
		  data: { module : module, elementid: elementid,val:val, id: id, action:action, teacher_id: $('#teacher_id').val() },
		  async:false,
          success:function(returnData){
			  returnData = $.trim(returnData);
			  //console.log(returnData);
			  if(returnData.trim()=='1' || returnData.trim()==1)
			  {
			    $('#'+elementid).next().remove();
				$('#'+elementid).after(err_html);
				$('#error_').val('1'); 
				$('#'+elementid).focus();
				//$('#'+elementid).val("");
			  }
			  
			  else {
				   if(module=='assign') {
				     $('#course_id').html(returnData);
			       }
				   else { 
				  $('#'+elementid).next().remove();
				  $('#error_').val(''); 
				   }
				  }   
			    
          }  
		 });      
	
}
function download_me(filename){  
	if(filename!=''){
	  window.location="download?filename="+filename;  
	 }
}

function delete_me(){
	   
	} 
	
 