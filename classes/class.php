<?php
#Author: Muhammad Rahil
#Email: itsmuhammad@outlook.com
#universal id: r4rahil
# 0092 334 522 7139
class myclass 
{   
  private $db_ = DATABASE;
  private $db_user_ = DATABASE_USER;
  private $db_pass_ = DATABASE_PASS;
  private $db_host_ = DATABASE_HOST;
  private $e_handling_ = array(false,false,false,false,false);
  private $root_dir_ = "\admin";	
  private $error_dir_ = "\\error_";	  
  private $error_log_file_name = "\\log_"; 
  
  public function __construct($db_='',$e_handling_='') {  
 
  	
	if($db_ === true)  
		  {
			  
			    $this->connectDb($this->db_,$this->db_user_,$this->db_pass_,$this->db_host_);
				 
		  }   
	
	   
	   
  }
  
  public function __destruct() {
	   mysqli_close($this->db_);
   }

  public function testInput($data='') {
	if($data=='' || $data==NULL) {return $data;} 
	$data = trim($data);
	//$data = str_replace(array(';','=','&&','||','^'),'', $data);
	$data = htmlspecialchars($data);
	$data = addslashes($data);
	return $data;
  
  }
    
  
  public function getTableColumn($table) {
	$table = "`".$table."`";
	$arrayData = array();
	$query = "SHOW COLUMNS FROM ".$table." ";
	$exe = mysqli_query($this->db_,$query);
	if(mysqli_num_rows($exe) > 0) {
	   while ($row = mysqli_fetch_assoc($exe)){ 
	         $arrayData[] = $row; }
	   }
	/* free result set */
    mysqli_free_result($exe);
	return $arrayData;
   
  }

  public function insertData($test='',$table,$column,$value) {
	 foreach($value as $key=>$v) { 
	         
			 $value[$key] = $this->testInput($v);
			 
	}
	$table = "`".$table."` ";
	$col = "`".implode("` , `", $column)."`";  
	$val = "'".implode("' , '", $value)."'";
    $query =  "INSERT INTO ".$table."(".$col.") VALUES(".$val.")"; 
	if($test != '' || $test != NULL){ 
	  return $query; 
	  }    
	$exe = mysqli_query($this->db_,$query); 
 
	if(count($exe) > '0'){ 
	  $msg = mysqli_insert_id($this->db_); 
	  }
	else{ 
	  $msg = '0'; 
	  }
  
   return $msg;  
   
  }
  
  public function getData($test='',$table,$column,$condition='',$orderBy='',$order='',$limit_condition='') {
    
	if($column == '*') { 
	  $col = $column;
	  }  
  else {
	 
	 $col = implode(",", $column);
	 }
	
	if($col!='*'){$col = str_replace(",","`,`",$col);$col = "`".$col."`";} 
	
    $query = "SELECT ".$col." FROM `".$table."` ";
   
   if($condition != ''){ 
     $query.= "WHERE ".$condition; 
	 }
  
  if ($order != NULL && $orderBy != NULL ){ 
     $query .= " ORDER BY ".$orderBy." ".$order; 
	 } 
  if($limit_condition!='') {$query .= $limit_condition." ";} 
	 if($test != '' || $test != NULL)
	 { return $query; } 
	//echo $query;exit;   
  $exe = mysqli_query($this->db_,$query); 
  $counter = mysqli_num_rows($exe);
  //echo "count is ".$count;
  $arrayData = array();  
  if($counter > 0 )   
	{ 
	 while($row = mysqli_fetch_object($exe))
		{ 
		
		$arrayData[] = $row; }
	}
	 
  else {}
	 
  /* free result set */
  mysqli_free_result($exe); 
  return $arrayData;
  }
  
  public function setData($test='',$table='',$column='',$value='',$condition){
		foreach($value as $key=>$v) // This loop is to make each and every input secure to some extent... .
			 {  
			 $value[$key] = $this->testInput($v);	
			 }
			 
   $table = "`".$table."`";
   $arr = array();
  
	   for($i=0; $i<(count($column));$i++)
		   {
			 $col =  "`".$column[$i]."`";
			 $val = "'".$value[$i]."'"; 
			 $arr[$i] = $col." = ".$val;
			
		   }
		   
   $finalData = implode(" , ", $arr);
   $query = "UPDATE ".$table." SET ".$finalData."";
   
   //if($condition != '') { 
   $query .= " WHERE ".trim($condition)." ";   
   //}
	
	if($test != '' || $test != NULL)
	{ return $query; } 
   $exe = mysqli_query($this->db_,$query);
  
	   if(count($exe) > '0')
		  { $msg = '1'; }
	   else
		   { $msg = '0'; }
		return $msg; 
  }
		
  public function deleteData($test='',$table='',$condition='') {
	   if($table=='' || $table==NULL){return "Table Name Missing !";}
	   $table = "`".$table."`";
		$query = "Delete FROM ".$table;
		if($condition != ''){ 
		$query .= " WHERE ".$condition." ";}
		 if($test != '' || $test != NULL){
			return $query; }
		
		$exe = mysqli_query($this->db_,$query);
			   
		if(count($exe) > '0'){ 
		  $msg = '1'; }
		else{ 
		  $msg = '0'; }
		return $msg; 
	  
  }
   
  public function getTableColumnName($table=''){
	   if($table=='' || $table==NULL){return "Table Name Missing !";}
	$columnData = $this->getTableColumn($table);
	$arrayData = array();
	
	foreach($columnData as $columnData)
			{ $arrayData[] = $columnData['Field']; }
	
	return $arrayData;
	
  }
   
  public function getDataFast($table='',$condition='') {
	$arrayData = array();
	$arrayData2[] = array();
	$columnData = $this->getTableColumn($table);
	
	foreach($columnData as $columnData)
			{ $arrayData[] = $columnData['Field']; }
	$arrayData2 = $this->getData($table,$arrayData,$condition,'','');
		
	 return $arrayData2;
	
  }
	
  public function truncateTable($test='',$table='') {
	  $query = "TRUNCATE TABLE ".$table." ";
	  if($test != '' || $test != NULL)
	{ return $query; }
	  $exe = mysqli_query($this->db_,$query);
	  if(count($exe) > '0')
	  { $msg = '1'; }
	else
	   { $msg = '0'; }
	return $msg; 
	  
	  
	  
  }
  
  public function getAscii($element=''){                           
	if(is_string($element))
	  { 
	   $ascii = ord($element); 
	   return $ascii;
	  }
  
	else if(is_array($element))
		{
		 $asciiArr =array();
		 for($i=0; $i<sizeof($element);$i++)
			  { $asciiArr[$i] = ord($element[$i]); }  
		 return $asciiArr;  
		 }
	  
  }
  
  public function toArrayPieces($str=''){ 
	$arr[]= array();
	return $arr = str_split($str); 
   
  }
  
  public function toArray($str='') { 
	$arr[]= array();
	$arr = explode(" ",$str);
	return $arr;
   
  }
   
  public function toString($arr=''){
	$str = implode(',',$arr);
	return $str; 
  }
  
  public function getDbTable($dbName=''){
	  if($dbName=='' || $dbName==NULL)
		{ return "Database Name Missing !"; }
  $table = array();
  $tblArr = array();
  
  $query = "SHOW TABLES FROM ".$dbName." ";
  $exe = mysqli_query($this->db_,$query);
  
  while($row = mysqli_fetch_array($exe))
		{ $table[] = $row; }
  
	  foreach($table as $table)
		{$tblArr[] =  $table[0];}
	  
  return $tblArr; 
  }
  
 public function doPagination($test='',$tbl='',$records='',$condition='',$orderCondition='',$design=''){
  ?>
  <style>
@charset "utf-8";
.paging {
	clear: both;
	padding: 20px 0;
	margin: 0% 1% 0% 0%;
	position: relative;
	font-size: 11px;
	line-height: 13px;
}
.paging span, .paging a {
	display: block;
	float: left;
	margin: 2px 2px 2px 0;
	padding: 6px 9px 5px 9px;
	text-decoration: none;
	width: auto;
	color: #fff;
	background: #555555;
}
.paging a:hover {
	color: #fff;
	background: -webkit-gradient(linear, 0% 0%, 0% 90%, from(#9AA9B1), to(#4b5a79));
}
.paging .current {
	padding: 6px 9px 5px 9px;
	background: -webkit-gradient(linear, 0% 0%, 0% 90%, from(#9AA9B1), to(#4b5a79));
	color: #fff;
}
</style>
  <?php
	  if($condition == NULL)
	   { 
		   $columnData = $this->getTableColumn($tbl);
		   foreach($columnData as $columnData)
			{ $arrayData[] = $columnData['Field']; }
			 
		  $condition = $arrayData['0']." IS NOT NULL ";
		  $orderyBy = $arrayData['0'];
	  }
	  else 
	  {
		  $columnData = $this->getTableColumn($tbl);
		   foreach($columnData as $columnData)
			{ $arrayData[] = $columnData['Field']; }
		  $orderyBy = $arrayData['0']; 
	  }
	  
	  $des = array();
	   if($design == '1') 
	   { 
	   //echo "i m ".$condition;exit;
		//$condition.= $arrayData['0']." IS NOT NULL ";
		   $page=''; $data = array();$data2 = array();$data3 = array();$data4 = array();
		   $url=  "http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
		  
		   extract($_REQUEST);
		   
			if(isset($paggeNo))
			  $from = "paggeNo=".$paggeNo;
			else
			  $from = "paggeNo=1";
	
   
	   if (strpos($url,'?') == true && strpos($url,'paggeNo') == true)
			{ 
			   
	   $currentpage = (int)$paggeNo;
		   }
		   
	   else
		{
		//echo "url is not having query string";
		$currentpage = 1;
		}
		
		 $dataCount = count($this -> getData($test,$tbl,array('*'),$condition,'',''));
		 $num_rows = $dataCount;
		 $totalpages = ceil($num_rows / $records);
   
		 $offset =($currentpage - 1) * $records;
		 // $condition .=" ORDER BY state_id DESC LIMIT $offset,$records"; 
		 if($orderCondition == NULL) {$orderCondition = " ORDER BY ".$orderyBy." DESC ";}  
		  $condition .= " ".$orderCondition." LIMIT $offset,$records"; 
		  $dat = $this -> getData($test,$tbl,array('*'),$condition,'','');
		  if($test != '' || $test != NULL)
		  { return $dat; } //  it will return the QUERY
		  //echo "condition iz ".$condition;exit;
		  if($dataCount>0 )
		  {
			  if ($currentpage > 1) {
	   
					$prevpage = $currentpage - 1;
					 
				  
				   $urla = str_replace($from,"paggeNo=".$prevpage, $url);
				   $urlb = str_replace($from, "paggeNo=1", $url);
				   if ( (strpos($url,'?') != true))
				   {
					 
					echo $a = '<a href="'.$urla.'?paggeNo='.$prevpage.'">Previous &raquo;</a>';
					 echo $b = '<a href="'.$urlb.'?paggeNo=1">First &raquo;</a>';
		   }
		   else if((strpos($url,'?') == true) && (strpos($url,'paggeNo') != true)) 
		   {
					 echo $a = '<a href="'.$urla.'&paggeNo='.$prevpage.'">Previous &raquo;</a>';
					 echo $b = '<a href="'.$urlb.'&paggeNo=1">First &raquo;</a>';
				  }
				   else
				   { echo $a = '<a href="'.$urla.'">&laquo; Previous</a>';
				  echo $b = '<a href="'.$urlb.'">&laquo; First</a>';} 
				   /*$a = '<a href="'.FRONT_END_PATH.'index.php?paggeNo='.$prevpage.'">&laquo; Previous</a>';
				   $b = '<a href="'.FRONT_END_PATH.'index.php?paggeNo=1">&laquo; First</a>'; */  
				   
			  } 
	  
	   
	  $range = 3;
	  for ($x = ($currentpage - $range); $x < (($currentpage + $range)  + 1); $x++) {
	   if (($x > 0) && ($x <= $totalpages)) {
		if ($x == $currentpage) {
		  echo "<span class='current'><b>$x</b></span> ";
	 
		  } else {
			  
			   $urlc = str_replace($from,"paggeNo=".$x, $url);
		   /*$c = '<a href="'.FRONT_END_PATH.'index.php?paggeNo='.$x.'">'.$x.'</a>';*/
		   
		   if ( (strpos($url,'?') != true))
		  {
		  
		   echo $c = '<a href="'.$urlc.'?paggeNo='.$x.'">'.$x.'</a>';
		  }
		  else if((strpos($url,'?') == true) && (strpos($url,'paggeNo') != true))
		  { echo $c = '<a href="'.$urlc.'&paggeNo='.$x.'">'.$x.'</a>';} 
		   else
		   {echo $c = '<a href="'.$urlc.'">'.$x.'</a>';}
		   
		  } // end else
		  } // end if 
	   } // end for
  
	   // if not on last page, show forward and last page links        
	   if ($currentpage != $totalpages) {
		  // get next page
		  $nextpage = $currentpage + 1;
		  
		  
		  
		
		   $urld = str_replace($from, "paggeNo=".$nextpage, $url);
		   $urle = str_replace($from, "paggeNo=".$totalpages, $url);
		  
		  if ( (strpos($url,'?') != true))
		  {
			  
		  echo $d = '<a href="'.$urld.'?paggeNo='.$nextpage.'">Next &raquo;</a>';
		   echo $e = '<a href="'.$urle.'?paggeNo='.$totalpages.'">Last &raquo;</a>';
		   
		  } 
		  else if((strpos($url,'?') == true) && (strpos($url,'paggeNo') != true))
		  {
		   echo $d = '<a href="'.$urld.'&paggeNo='.$nextpage.'">Next &raquo;</a>';
		   echo $e = '<a href="'.$urle.'&paggeNo='.$totalpages.'">Last &raquo;</a>';	
		  }
		   
		  else
		  {echo $d = '<a href="'.$urld.'">Next &raquo;</a>';
		   echo  $e = '<a href="'.$urle.'">Last &raquo;</a>';}
		   
		   /*$d = '<a href="'.FRONT_END_PATH.'index.php?paggeNo='.$nextpage.'">Next &raquo;</a>';
		   $e = '<a href="'.FRONT_END_PATH.'index.php?paggeNo='.$totalpages.'">Last &raquo;</a>';*/
		  
		
		  
	   } // end if
	   
	   
	 }
			  
  return $dat;
	  
	   }
	   else
	   { 
	  // echo "condition iz ".$condition;exit;
		//$condition.= $arrayData['0']." IS NOT NULL ";
		   $page=''; $data = array();$data2 = array();$data3 = array();$data4 = array();
		   $url=  "http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
		  
		   extract($_REQUEST);
		   
			if(isset($paggeNo))
			  $from = "paggeNo=".$paggeNo;
			else
			  $from = "paggeNo=1";
	
   
	   if (strpos($url,'?') == true && strpos($url,'paggeNo') == true)
			{ 
			   
	   $currentpage = (int)$paggeNo;
		   }
		   
	   else
		{
		//echo "url is not having query string";
		$currentpage = 1;
		}
		   //echo "condition iz ".$condition;exit;
		 $dataCount = count($this -> getData($test,$tbl,array('*'),$condition,'',''));
		  
		// echo "data countt is ".$dataCount;exit;
		 $num_rows = $dataCount;
		 $totalpages = ceil($num_rows / $records);
   
		 $offset =($currentpage - 1) * $records;
		 // $condition .=" ORDER BY state_id DESC LIMIT $offset,$records"; 
		 if($orderCondition == NULL) {$orderCondition = " ORDER BY ".$orderyBy." DESC ";}  
		  $condition .= " ".$orderCondition." LIMIT $offset,$records"; 
		  //echo "condition iz ".$condition;exit;
		  $dat = $this -> getData($test,$tbl,array('*'),$condition,'','');
		  if($test != '' || $test != NULL)
		  { return $dat; } //  it will return the QUERY
		  
		 
			  
  return $dat; 
	  
	   } 
  
  
  }
    
	
  public function doPagination_query($test='',$query,$records,$design=''){
  ?>
  <style>
@charset "utf-8";
.paging {
	clear: both;
	padding: 20px 0;
	margin: 0% 1% 0% 0%;
	position: relative;
	font-size: 11px;
	line-height: 13px;
}
.paging span, .paging a {
	display: block;
	float: left;
	margin: 2px 2px 2px 0;
	padding: 6px 9px 5px 9px;
	text-decoration: none;
	width: auto;
	color: #fff;
	background: #555555;
}
.paging a:hover {
	color: #fff;
	background: -webkit-gradient(linear, 0% 0%, 0% 90%, from(#9AA9B1), to(#4b5a79));
}
.paging .current {
	padding: 6px 9px 5px 9px;
	background: -webkit-gradient(linear, 0% 0%, 0% 90%, from(#9AA9B1), to(#4b5a79));
	color: #fff;
}
</style>
  <?php
	    
		/*   $columnData = $this->getTableColumn($tbl);
		   foreach($columnData as $columnData)
			{ $arrayData[] = $columnData['Field']; }
			 
		  $condition = $arrayData['0']." IS NOT NULL ";
		  $orderyBy = $arrayData['0'];
	   */ 
	 
	  
	  $des = array();
	   if($design == '1') 
	   { 
	   //echo "i m ".$condition;exit;
		//$condition.= $arrayData['0']." IS NOT NULL ";
		   $page=''; $data = array();$data2 = array();$data3 = array();$data4 = array();
		   $url=  "http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
		  
		   extract($_REQUEST);
		   
			if(isset($paggeNo))
			  $from = "paggeNo=".$paggeNo;
			else
			  $from = "paggeNo=1";
	
   
	   if (strpos($url,'?') == true && strpos($url,'paggeNo') == true)
			{ 
			   
	   $currentpage = (int)$paggeNo;
		   }
		   
	   else
		{
		//echo "url is not having query string";
		$currentpage = 1;
		} 
		
		 //$dataCount = count($this -> getData($test,$tbl,array('*'),$condition,'',''));
		 $dataCount = count($this -> query($test,$query));
		 $num_rows = $dataCount;
		 $totalpages = ceil($num_rows / $records);
   
		 $offset =($currentpage - 1) * $records;
		 // $condition .=" ORDER BY state_id DESC LIMIT $offset,$records"; 
		   
		  $condition = " LIMIT $offset,$records"; 
		  //$dat = $this -> getData($test,$tbl,array('*'),$condition,'','');
		  $dat = $this -> query($test,$query);
		  
		  if($test != '' || $test != NULL)
		  { return $dat; } //  it will return the QUERY
		  //echo "condition iz ".$condition;exit;
		  if($dataCount>0 )
		  {
			  if ($currentpage > 1) {
	   
					$prevpage = $currentpage - 1;
					 
				  
				   $urla = str_replace($from,"paggeNo=".$prevpage, $url);
				   $urlb = str_replace($from, "paggeNo=1", $url);
				   if ( (strpos($url,'?') != true))
				   {
					 
					echo $a = '<a href="'.$urla.'?paggeNo='.$prevpage.'">Previous &raquo;</a>';
					 echo $b = '<a href="'.$urlb.'?paggeNo=1">First &raquo;</a>';
		   }
		   else if((strpos($url,'?') == true) && (strpos($url,'paggeNo') != true)) 
		   {
					 echo $a = '<a href="'.$urla.'&paggeNo='.$prevpage.'">Previous &raquo;</a>';
					 echo $b = '<a href="'.$urlb.'&paggeNo=1">First &raquo;</a>';
				  }
				   else
				   { echo $a = '<a href="'.$urla.'">&laquo; Previous</a>';
				  echo $b = '<a href="'.$urlb.'">&laquo; First</a>';} 
				   /*$a = '<a href="'.FRONT_END_PATH.'index.php?paggeNo='.$prevpage.'">&laquo; Previous</a>';
				   $b = '<a href="'.FRONT_END_PATH.'index.php?paggeNo=1">&laquo; First</a>'; */  
				   
			  } 
	  
	   
	  $range = 3;
	  for ($x = ($currentpage - $range); $x < (($currentpage + $range)  + 1); $x++) {
	   if (($x > 0) && ($x <= $totalpages)) {
		if ($x == $currentpage) {
		  echo "<span class='current'><b>$x</b></span> ";
	 
		  } else {
			  
			   $urlc = str_replace($from,"paggeNo=".$x, $url);
		   /*$c = '<a href="'.FRONT_END_PATH.'index.php?paggeNo='.$x.'">'.$x.'</a>';*/
		   
		   if ( (strpos($url,'?') != true))
		  {
		  
		   echo $c = '<a href="'.$urlc.'?paggeNo='.$x.'">'.$x.'</a>';
		  }
		  else if((strpos($url,'?') == true) && (strpos($url,'paggeNo') != true))
		  { echo $c = '<a href="'.$urlc.'&paggeNo='.$x.'">'.$x.'</a>';} 
		   else
		   {echo $c = '<a href="'.$urlc.'">'.$x.'</a>';}
		   
		  } // end else
		  } // end if 
	   } // end for
  
	   // if not on last page, show forward and last page links        
	   if ($currentpage != $totalpages) {
		  // get next page
		  $nextpage = $currentpage + 1;
		  
		  
		  
		
		   $urld = str_replace($from, "paggeNo=".$nextpage, $url);
		   $urle = str_replace($from, "paggeNo=".$totalpages, $url);
		  
		  if ( (strpos($url,'?') != true))
		  {
			  
		  echo $d = '<a href="'.$urld.'?paggeNo='.$nextpage.'">Next &raquo;</a>';
		   echo $e = '<a href="'.$urle.'?paggeNo='.$totalpages.'">Last &raquo;</a>';
		   
		  } 
		  else if((strpos($url,'?') == true) && (strpos($url,'paggeNo') != true))
		  {
		   echo $d = '<a href="'.$urld.'&paggeNo='.$nextpage.'">Next &raquo;</a>';
		   echo $e = '<a href="'.$urle.'&paggeNo='.$totalpages.'">Last &raquo;</a>';	
		  }
		   
		  else
		  {echo $d = '<a href="'.$urld.'">Next &raquo;</a>';
		   echo  $e = '<a href="'.$urle.'">Last &raquo;</a>';}
		   
		   /*$d = '<a href="'.FRONT_END_PATH.'index.php?paggeNo='.$nextpage.'">Next &raquo;</a>';
		   $e = '<a href="'.FRONT_END_PATH.'index.php?paggeNo='.$totalpages.'">Last &raquo;</a>';*/
		  
		
		  
	   } // end if
	   
	   
	 }
			  
  return $dat;
	  
	   }
	   else
	   { 
	  // echo "condition iz ".$condition;exit;
		//$condition.= $arrayData['0']." IS NOT NULL ";
		   $page=''; $data = array();$data2 = array();$data3 = array();$data4 = array();
		   $url=  "http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
		  
		   extract($_REQUEST);
		   
			if(isset($paggeNo))
			  $from = "paggeNo=".$paggeNo;
			else
			  $from = "paggeNo=1";
	
   
	   if (strpos($url,'?') == true && strpos($url,'paggeNo') == true)
			{ 
			   
	       $currentpage = (int)$paggeNo;
		   }
		   
	   else
		{  
		//echo "url is not having query string";
		$currentpage = 1;
		}
		   
		 $dataCount = count($this -> query($test,$query)); 

		 $num_rows = $dataCount;
		 $totalpages = ceil($num_rows / $records);
   
		 $offset =($currentpage - 1) * $records;
		 
		 $condition = " LIMIT $offset,$records"; 
		 $query.=$condition;
		 $dat = $this -> query($test,$query);  
		  if($test != '' || $test != NULL)
		  { return $dat; } //  it will return the QUERY
		  
		 
			  
  return $dat; 
	  
	   }  
  
  
  }
  
  
  public function connectDb($dbName='',$dbUsername='',$dbPassword='',$dbHost=''){
	  
	  					  
		  $DB_USER = $dbUsername;
		  $DB_PASSWORD = $dbPassword;
		  $DB_NAME = $dbName;
		  $DB_HOST = $dbHost; 
	  
	  //echo $DB_HOST." ".$DB_USER." ".$DB_PASSWORD." ".$DB_NAME;  
	  //var_dump(function_exists('mysqli_connect'));
	  $this->db_ = mysqli_connect($DB_HOST,$DB_USER,$DB_PASSWORD,$DB_NAME);
		  if (mysqli_connect_errno()) { printf("Connect failed: %s\n", mysqli_connect_error()); exit(); } 
		   
  }
  

  function imageResize($originFolder, $destinyFolder,$imageName,$desiredWidth='') {
	  if( (!is_dir($originFolder)) || !(file_exists($originFolder.$imageName)) || (!is_dir($destinyFolder)) )
	   
	   { 
	   if( !is_dir($originFolder) ) { echo "<center><font style='color:red;'>Folder '".$originFolder."' doesn't exist </font></center>"; return 0;}
	   
	   else if( !(file_exists($originFolder.$imageName)) ) { echo "<center><font style='color:red;' >File '".$imageName."' Not Found </font></center>"; return 0; }
	   
	   else if( !is_dir($destinyFolder) ) {echo "<center><font style='color:red;' >Folder '".$destinyFolder."' doesn't exist </font></center>"; return 0; } 
	   
	   else 
	   { echo "<center><font style='color:red;'>Some Error While Image resizing</font></center>"; return 0; }
		
	   }
	   
	// open the directory
	   $dir = opendir( $originFolder );
	   
	// loop through it, looking for any/all JPG files:
	 $fname = $imageName;
	  // parse path for the extension
	  $info = pathinfo($originFolder . $fname);
	  // continue only if this is a JPEG image
	  if ( strtolower($info['extension']) == 'jpg' || $info['extension'] == 'PNG' || $info['extension'] == 'jpeg' || $info['extension'] == 'gif' || $info['extension'] == 'png' ) 
	  {
		  
	   // echo "Creating thumbnail for {$fname} <br />";
		// load image and get image size
		if($info['extension'] == 'JPEG' || $info['extension'] == 'jpeg' || $info['extension'] == 'JPG' || $info['extension'] == 'jpg')
		 { $img = imagecreatefromjpeg( "{$originFolder}{$fname}" ); }
		 else if ($info['extension'] == 'PNG' || $info['extension'] == 'png')
		{ $img = imagecreatefrompng( "{$originFolder}{$fname}" ); }
		else if ($info['extension'] == 'GIF' || $info['extension'] == 'gif')
		{ $img = imagecreatefromgif( "{$originFolder}{$fname}" ); }
		 else
		 { $img = imagecreatefromjpeg( "{$originFolder}{$fname}" ); }
		 
		$width = imagesx( $img );
		$height = imagesy( $img ); 
		if($desiredWidth =='')
		{$desiredWidth = '600';}
		if( (!is_numeric($desiredWidth)) )
		  { echo "<center><font style='color:red;'>Thumbnail width can only be numeric e.g 700</font></center>"; return 0; }
		// calculate thumbnail size
		$new_width = $desiredWidth;
		$new_height = floor( $height * ( $desiredWidth / $width ) );
		// create a new tempopary image
		$tmp_img = imagecreatetruecolor( $new_width, $new_height );
		// copy and resize old image into new image 
		imagecopyresized( $tmp_img, $img, 0, 0, 0, 0, $new_width, $new_height, $width, $height );
		// save thumbnail into a file
		imagejpeg( $tmp_img, "{$destinyFolder}{$fname}" );
	  }
	  else { echo "<center><font style='color:red;'>Invalid file format (jpg, jpeg, png and gif are allowed only )</font></center>"; return 0;}
	// close the directory
	closedir( $dir );
  
  }

  public function execute($query){
	  mysqli_query($this->db_,$query);  
  }
   
  public function query($test='',$query=''){
	  
	  if($test != '' || $test != NULL)
	 { return $query; } 
	 if($query=='' || $query==NULL)
	 {
		return "Query Missing !"; 
	 }
	 
	  $arrayData = array();
	  $result = mysqli_query($this->db_,$query);
  $counter = mysqli_num_rows($result);
  $arrayData = array();
  if($counter > 0 ) 
	{
	 while($row = mysqli_fetch_object($result))
		{ $arrayData[] = $row; }
	}
	
  else {}
	  
  return $arrayData;
  } 
  
  // upload file functions 
  public function compressImage($ext,$uploadedfile,$path,$actual_image_name,$newwidth) {
//echo $uploadedfile." PP";die();
if($ext=="jpg" || $ext=="jpeg" || $ext=="JPG" || $ext=="JPEG")
{
$src = imagecreatefromjpeg($uploadedfile);
}
else if($ext=="png" || $ext=="PNG")
{
$src = imagecreatefrompng($uploadedfile);
}
else if($ext=="gif" || $ext=="GIF")
{
$src = imagecreatefromgif($uploadedfile);
}
else
{
$src = imagecreatefrombmp($uploadedfile);
}

list($width,$height)=getimagesize($uploadedfile);
$newheight=($height/$width)*$newwidth;
$tmp=imagecreatetruecolor($newwidth,$newheight);
imagecopyresampled($tmp,$src,0,0,0,0,$newwidth,$newheight,$width,$height);
$filename = $path.$newwidth.'_'.$actual_image_name; //PixelSize_TimeStamp.jpg
imagejpeg($tmp,$filename,100);
imagedestroy($tmp);
return $filename;}
  public function getExtension($str){
$i = strrpos($str,".");
if (!$i)
{
return "";
}
$l = strlen($str) - $i;
$ext = substr($str,$i+1,$l); 
return $ext;} 

  public function upload_file($file='',$upload_path='',$allowed_extension='',$allowed_size='',$type='',$widthArray='',$filecounter) {
	$allowed_size_all  = array("jpg", "png", "gif", "bmp","jpeg","doc", "docx", "ppt", "pptx","xls","xlsx","txt","mp3","mp4","mov"); 
$filecounter = $filecounter!=''?$filecounter:'0';	
//echo "<pre>Here: ";print_r($file); 
$file_ = $file;
if($upload_path=='') {$path = "uploads/"; } else {$path = $upload_path;}   

// check directory existance
if( !(is_dir($upload_path)) )   
  { echo "Invalid Directory ".$upload_path;exit; }   
// /check directory existance
 
$filename = $file_['name'];      
$file_simple_name = explode(".",$filename);    
$size = $file_['size'];
//echo $size;exit;
$uploadedfile = $file_['tmp_name']; 

if($type=='image')
 {   
   if($allowed_extension=='') {$allowed_extension = $allowed_size_all; }  
   if($allowed_size=='') {$allowed_size=1024*1024*1;} else {$allowed_size=1024*1024*$allowed_size;}  
 }
 else if($type=='file') { 
	      if($allowed_extension=='') {$allowed_extension = $allowed_size_all; }
          if($allowed_size=='') {$allowed_size=1024*1024*1;} else {$allowed_size=1024*1024*$allowed_size;}  
 }
 else if($type=='audio') { 
          if($allowed_extension=='') {$allowed_extension = $allowed_size_all; }
          if($allowed_size=='') {$allowed_size=1024*1024*1;} else {$allowed_size=1024*1024*$allowed_size;} 
		  }
 else if($type=='video') { 
          if($allowed_extension=='') {$allowed_extension = $allowed_size_all; } 
          if($allowed_size=='') {$allowed_size=1024*1024*1;} else {$allowed_size=1024*1024*$allowed_size;} 
		  }
 else { return "Invalid file Type"; exit;}      

 //echo "image";   


// 
if(strlen($filename))     
{
	$ext = strtolower($this->getExtension($filename));  
	 //echo $ext;exit;
	$actual_file_name = @$filecounter."_".time()."_".$file_simple_name[0].".".$ext;   
	if(in_array(strtolower($ext),$allowed_extension))  
	{  
		if($size<($allowed_size)) // Image size max 1 MB default size is 1 mb
		{ 

			//if($widthArray=='') { $widthArray = array(200,100,50); } //You can change dimension here. 
			if(@$widthArray!='' && (sizeof($widthArray)>0) && $type=='image')     
			{
				foreach($widthArray as $newwidth)
				{  
				$filename=$this->compressImage($ext,$uploadedfile,$path,$actual_file_name,$newwidth);
				}
			}
			//Original Image 
			if(move_uploaded_file($uploadedfile, $path.$actual_file_name))
			{
			 return $actual_file_name."_-_".$type;  
			}
			else
			 return $filename."_-_".$type."_-_failed";    
		}
		else
		return $filename."_-_".$type."_-_exceded size ".($allowed_size/1024/1024)." MB";     
	}  
	else
	return $filename."_-_".$type."_-_Invalid file format";      
}
else
return "Please select image";  

 }

  public function reArrayFiles(&$file_post) {   

    $file_ary = array();
    $file_count = count($file_post['name']);
    $file_keys = array_keys($file_post);

    for ($i=0; $i<$file_count; $i++) {
        foreach ($file_keys as $key) {
            $file_ary[$i][$key] = $file_post[$key][$i];
        }
    } 

    return $file_ary; } 
  // /upload file functions 
  
  public function download_me($file){
	   if (file_exists($file))
		  {
			  header("Cache-Control: public");
			  header("Content-Description: File Transfer");
			  header("Content-Disposition: attachment; filename=".$file."");
			  header("Content-Transfer-Encoding: binary");
			  header("Content-Type: binary/octet-stream");
			  while (ob_get_level()) {
			   ob_end_clean();
			  }
			  readfile($file);
			  exit(); 
		 }
	  }
} 
$myObj = new myclass(true,array(true,true,(@$_REQUEST['e_msg_'] == true ? true : true),(@$_REQUEST['e_file_'] == true ? true : true),(@$_REQUEST['e_line_'] == true ? true : true)));    
?> 
