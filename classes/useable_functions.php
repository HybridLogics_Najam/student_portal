<?php
/*  Comman functions of php 
 * Author : Bilal Kabeer Butt
 * Website : www.gegabyte.org
 * email : bilalbutt@gmail.com
*/

##############################################################################
/* Useable Variable */
define("PRES","<pre>");
define("pres","<pre>");

define("PREE","</pre>");
define("pree","</pre>");

define("br","<br />");
define("BR","<br />");

define("br2","<br /><br />");
define("BR2","<br /><br />");

define("hr","<hr />");
define("HR","<hr />");
##############################################################################

##############################################################################
/* FOR FINDING WHETHER THE PAGE / SITE IS RUNNING ON LOCALHOST OR WEBSERVER */
function online(){
	if (strtoupper($_SERVER['HTTP_HOST']) == 'LOCALHOST')
	{
		return false;
	}
	else
	{
		return true;
	}	
}
##############################################################################

##############################################################################
/* FOR FINDING USER BROWSER */
function getBrowser()
    {
        $u_agent = $_SERVER['HTTP_USER_AGENT'];
        $ub = '';
        if(preg_match('/MSIE/i',$u_agent))
        {
            $ub = "Internet Explorer";
        }
        elseif(preg_match('/Firefox/i',$u_agent))
        {
            $ub = "Mozilla Firefox";
        }
        elseif(preg_match('/Chrome/i',$u_agent))
        {
            $ub = "Google Chrome";
        }
        elseif(preg_match('/Safari/i',$u_agent))
        {
            $ub = "Apple Safari";
        }
        elseif(preg_match('/Flock/i',$u_agent))
         {
            $ub = "Flock";
        }
        elseif(preg_match('/Opera/i',$u_agent))
        {
            $ub = "Opera";
        }
        elseif(preg_match('/Netscape/i',$u_agent))
        {
            $ub = "Netscape";
        }
        return $ub;
    }
##############################################################################

##############################################################################
/* VISUAL BASIC 6 LEFT$ EVQUALENT OF PHP */
function Left($str,$len)
{
	$length=strlen($str);
	if ( $len > $length ) $len=$length;
		else if ( $len <= 0 ) $new = $str;
		else
		{
			for ($i=0;$i<$len;$i++)
			{
				$temp[]=$str{$i};
			}
		$new = implode ("",$temp);
	}
	return ($new);
}
##############################################################################

##############################################################################
/* VISUAL BASIC 6 MID$ EVQUALENT OF PHP */
function Mid($str,$end)
{
	$t_length = round(strlen($str) / 2);
	$new = substr ($str, $t_length, $end);
	return $new;
}
##############################################################################

##############################################################################
/* VISUAL BASIC 6 RIGTH$ EVQUALENT OF PHP */
function Right($str,$len)
{
	$rest = substr($str, -$len);
	return $rest;
}
##############################################################################

##############################################################################
/* REMOVE @ AND EVERYTHING AFTER IT FROM EMAIL email@abc.com */
function RemoveAt($UserName)
{
	$username_spt = explode("@",$UserName);	
	return $username_spt[0];
}
##############################################################################

##############################################################################
/* REMOVE LEADING ZEROS FROM GIVEN STRING / NUMBER 000a46d00da4 CHANGES TO a46d00da4 */
function RemoveZeros($number)
{
	$length=strlen($number)-1;
	$new_number = '';
	$number_found = '';
	
	if (is_numeric($number))
	{
		for ($a=0;$a<=$length;$a++)
		{
			$pattern = '/[A-Za-z1-9]+/i';
			$sub = Mid($number,$a,1);
			if (preg_match($pattern,$sub) == 1)
			{
				$new_number .= Mid($number,$a,1);
				$number_found = 1;
			}
			else
			{
				if ($number_found == 1)
				{
					$new_number .= Mid($number,$a,1);
				}
			}
		}		
	}
	else
	{
		$new_number = $number;
	}
	return $new_number;
}
##############################################################################

##############################################################################
/* For Writing Log Files */
function WriteDebug($context,$spc){
	
		//	Use these $spc values
		//	0 					=	Normal (With Date & Time)
		//	1 					=	3 Tab Indented (With Date & Time)
		//	2 					=	1 Space (Without Date & Time)
		//	3 					=	1 Tab Indented (With Date & Time)
		//	Any other number	=	3 Tab Indented (With Date & Time)
		
		global $fp;
		$fp = fopen('log.txt', 'a');
		$DateTime =  date("Y-m-d H:i:s");
		if ($context == "######")
		{
			$context_final = $context . $context. $context ."\n";	
		}
		else
		{
			if ($spc == 0)
			{
				$context_final = $DateTime . ' : ' . $context ."\n";
			}
			else if ($spc == 1)
			{
				$context_final = "			" . $DateTime . ' : ' . $context ."\n";
			}
			else if ($spc == 2)
			{
				$context_final = ' ' . $context ."\n";
			}
			else if ($spc == 3)
			{
				$context_final = "		" . $DateTime . ' : ' . $context ."\n";
			}
			else
			{
				$context_final = "			" . $DateTime . ' : ' . $context ."\n";
			}
		}
		fwrite($fp,$context_final);
		fclose($fp);
	}
##############################################################################

##############################################################################
/* For spliting the given string on given character and returning the first part only */
function explod($str,$character_to_split_on){
	$strn = explode($character_to_split_on,$str);
	return $strn[1];
}
##############################################################################

##############################################################################
function ReplaceUTF($str){
	/* Readable */
	$str =  preg_replace("/&lsquo;/i","'",$str);
	$str =  preg_replace("/&rsquo;/i","'",$str);
	$str =  preg_replace("/&sbquo;/i","‚",$str);
	$str =  preg_replace("/&ldquo;/i","\"",$str);
	$str =  preg_replace("/&rdquo;/i","\"",$str);
	$str =  preg_replace("/&bdquo;/i","\"",$str);
	$str =  preg_replace("/&trade;/i","™",$str);
	$str =  preg_replace("/&quot;/i","\"",$str);
	$str =  preg_replace("/&amp;/i","&",$str);
	$str =  preg_replace("/&frasl;/i","/",$str);
	$str =  preg_replace("/&lt;/i","<",$str);
	$str =  preg_replace("/&gt;/i",">",$str);
	$str =  preg_replace("/&nbsp;/i"," ",$str);
	$str =  preg_replace("/&copy;/i","©",$str);
	$str =  preg_replace("/&reg;/i","®",$str);
	$str =  preg_replace("/&acute;/i","´",$str);
	$str =  preg_replace("/&laquo;/i","«",$str);
	$str =  preg_replace("/&raquo;/i","»",$str);
	/* Readable */	

	/* Without Leading Zero &#01; | &#92; */
	$str =  preg_replace("/&#x2122;/i","™",$str);
	$str =  preg_replace("/&#01;/i","",$str);
	$str =  preg_replace("/&#02;/i","",$str);
	$str =  preg_replace("/&#03;/i","",$str);
	$str =  preg_replace("/&#04;/i","",$str);
	$str =  preg_replace("/&#05;/i","",$str);
	$str =  preg_replace("/&#06;/i","",$str);
	$str =  preg_replace("/&#07;/i","",$str);
	$str =  preg_replace("/&#08;/i","",$str);
	$str =  preg_replace("/&#11;/i","",$str);
	$str =  preg_replace("/&#32;/i"," ",$str);
	$str =  preg_replace("/&#33;/i","!",$str);
	$str =  preg_replace("/&#34;/i","\"",$str);
	$str =  preg_replace("/&#35;/i","#",$str);
	$str =  preg_replace("/&#36;/i","$",$str);
	$str =  preg_replace("/&#37;/i","%",$str);
	$str =  preg_replace("/&#38;/i","&",$str);
	$str =  preg_replace("/&#39;/i","'",$str);
	$str =  preg_replace("/&#40;/i","(",$str);
	$str =  preg_replace("/&#41;/i",")",$str);
	$str =  preg_replace("/&#42;/i","*",$str);
	$str =  preg_replace("/&#43;/i","+",$str);
	$str =  preg_replace("/&#45;/i","-",$str);
	$str =  preg_replace("/&#46;/i",".",$str);
	$str =  preg_replace("/&#47;/i","/",$str);
	$str =  preg_replace("/&#58;/i",":",$str);
	$str =  preg_replace("/&#59;/i",";",$str);
	$str =  preg_replace("/&#60;/i","<",$str);
	$str =  preg_replace("/&#61;/i","=",$str);
	$str =  preg_replace("/&#62;/i",">",$str);
	$str =  preg_replace("/&#63;/i","?",$str);
	$str =  preg_replace("/&#64;/i","@",$str);
	$str =  preg_replace("/&#91;/i","[",$str);
	$str =  preg_replace("/&#92;/i","\\",$str);
	$str =  preg_replace("/&#93;/i","]",$str);
	$str =  preg_replace("/&#94;/i","^",$str);
	$str =  preg_replace("/&#95;/i","_",$str);
	$str =  preg_replace("/&#96;/i","`",$str);
	/* Without Leading Zero &#01; | &#92; */

	/* With Leading Zero Three Digits &#001; | &#092; */
	$str =  preg_replace("/&#001;/i","",$str);
	$str =  preg_replace("/&#002;/i","",$str);
	$str =  preg_replace("/&#003;/i","",$str);
	$str =  preg_replace("/&#004;/i","",$str);
	$str =  preg_replace("/&#005;/i","",$str);
	$str =  preg_replace("/&#006;/i","",$str);
	$str =  preg_replace("/&#007;/i","",$str);
	$str =  preg_replace("/&#008;/i","",$str);
	$str =  preg_replace("/&#011;/i","",$str);
	$str =  preg_replace("/&#032;/i"," ",$str);
	$str =  preg_replace("/&#033;/i","!",$str);
	$str =  preg_replace("/&#034;/i","\"",$str);
	$str =  preg_replace("/&#035;/i","#",$str);
	$str =  preg_replace("/&#036;/i","$",$str);
	$str =  preg_replace("/&#037;/i","%",$str);
	$str =  preg_replace("/&#038;/i","&",$str);
	$str =  preg_replace("/&#039;/i","'",$str);
	$str =  preg_replace("/&#040;/i","(",$str);
	$str =  preg_replace("/&#041;/i",")",$str);
	$str =  preg_replace("/&#042;/i","*",$str);
	$str =  preg_replace("/&#043;/i","+",$str);
	$str =  preg_replace("/&#045;/i","-",$str);
	$str =  preg_replace("/&#046;/i",".",$str);
	$str =  preg_replace("/&#047;/i","/",$str);
	$str =  preg_replace("/&#058;/i",":",$str);
	$str =  preg_replace("/&#059;/i",";",$str);
	$str =  preg_replace("/&#060;/i","<",$str);
	$str =  preg_replace("/&#061;/i","=",$str);
	$str =  preg_replace("/&#062;/i",">",$str);
	$str =  preg_replace("/&#063;/i","?",$str);
	$str =  preg_replace("/&#064;/i","@",$str);
	$str =  preg_replace("/&#091;/i","[",$str);
	$str =  preg_replace("/&#092;/i","\\",$str);
	$str =  preg_replace("/&#093;/i","]",$str);
	$str =  preg_replace("/&#094;/i","^",$str);
	$str =  preg_replace("/&#095;/i","_",$str);
	$str =  preg_replace("/&#096;/i","`",$str);
	/* With Leading Zero Three Digits &#001; | &#092; */
	
	/* Three Digits &#123; */
	$str =  preg_replace("/&#123;/i","{",$str);
	$str =  preg_replace("/&#124;/i","|",$str);
	$str =  preg_replace("/&#125;/i","}",$str);
	$str =  preg_replace("/&#152;/i","",$str);
	$str =  preg_replace("/&#153;/i","",$str);
	$str =  preg_replace("/&#154;/i","",$str);
	$str =  preg_replace("/&#155;/i","",$str);
	$str =  preg_replace("/&#156;/i","",$str);
	$str =  preg_replace("/&#157;/i","",$str);
	$str =  preg_replace("/&#158;/i","",$str);
	$str =  preg_replace("/&#159;/i","",$str);
	$str =  preg_replace("/&#160;/i"," ",$str);
	$str =  preg_replace("/&#169;/i","©",$str);
	$str =  preg_replace("/&#171;/i","«",$str);
	$str =  preg_replace("/&#174;/i","®",$str);
	$str =  preg_replace("/&#180;/i","´",$str);
	$str =  preg_replace("/&#187;/i","»",$str);
	/* Three Digits &#123; */

	/* Four Digits &#1234; */
	$str =  preg_replace("/&#8211;/i","-",$str);
	$str =  preg_replace("/&#8230;/i","…",$str);
	$str =  preg_replace("/&#8212;/i","—",$str);
	/* Four Digits &#1234; */	
	return $str;
}
##############################################################################

##############################################################################
function RepEmo($fnd,$replacewith,$str){
	$fnd2 = '/' . $fnd  . '/';
	$replace_txt = '<img src="' . $replacewith . '.gif">';
	$repd = preg_replace($fnd2,$replace_txt,$str);
	return $repd;
}
##############################################################################

##############################################################################
function RemoveP($text,$replace_with){
	$str = '<p>My First Media Asset Description</p><p style="position:relative;" class="asa"> My Second Media Asset Description</p>';
	$fnd = "/\<p (.*?)\>|\<p\>|\<\/p\>/i";
	$new_str = preg_replace($fnd,$replace_with,$text);
	return $new_str;
}
##############################################################################
?>