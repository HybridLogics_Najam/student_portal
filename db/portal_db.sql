-- phpMyAdmin SQL Dump
-- version 4.0.10.18
-- https://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Apr 07, 2017 at 11:37 AM
-- Server version: 10.1.22-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `privauniletd_portal_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `attachment_tbl`
--

CREATE TABLE IF NOT EXISTS `attachment_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `assign_id` int(11) NOT NULL,
  `create_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` int(1) DEFAULT '1' COMMENT '1=active,2=delete,0=deactive',
  `title` varchar(255) NOT NULL,
  `desc` varchar(255) DEFAULT NULL,
  `type` tinyint(1) DEFAULT NULL COMMENT '1=image,2=files(doc,xls,ppt,txt etc), 3=audio,4=video',
  `created_by` int(11) DEFAULT NULL,
  `visibility_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT 'This is the expiry date for attachment, after this date, attachment will become invisible',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=32 ;

--
-- Dumping data for table `attachment_tbl`
--

INSERT INTO `attachment_tbl` (`id`, `assign_id`, `create_date`, `status`, `title`, `desc`, `type`, `created_by`, `visibility_date`) VALUES
(18, 6, '2017-03-09 18:29:46', 1, '1_1484427075_IMG_0002.jpg', NULL, 1, 1, '2017-03-08 19:00:00'),
(19, 5, '2017-01-14 20:51:36', 1, '1_1484427096_New Text Document.txt', NULL, 2, 1, NULL),
(20, 4, '2017-03-08 18:05:42', 1, '1_1484427102_IMG_0041.jpg', NULL, 1, 1, '2017-03-06 19:00:00'),
(21, 6, '2017-01-14 20:52:27', 1, '1_1484427147_IMG_0002.jpg', NULL, 1, 1, NULL),
(22, 6, '2017-01-14 20:52:27', 1, '2_1484427147_IMG_0013.jpg', NULL, 1, 1, NULL),
(23, 6, '2017-01-14 20:52:34', 1, '1_1484427154_IMG_0041.jpg', NULL, 1, 1, NULL),
(24, 6, '2017-01-14 20:52:56', 1, '1_1484427176_01.mp3', NULL, 2, 1, NULL),
(25, 5, '2017-02-18 04:42:29', 1, '1_1487392949_body-bg.png', NULL, 1, NULL, NULL),
(26, 7, '2017-03-09 18:29:06', 1, '1_1488989304_Capturessss - Copy.png', NULL, 1, NULL, '2017-03-07 19:00:00'),
(27, 7, '2017-03-08 17:37:48', 1, '2_1488989304_Capturessss.png', NULL, 1, NULL, '2017-03-14 19:00:00'),
(29, 6, '2017-03-09 18:31:29', 1, '1_1489084289_message_avatar2.png', NULL, 1, NULL, '2017-03-30 19:00:00'),
(30, 6, '2017-03-09 18:41:28', 1, '1_1489084888_message_avatar1.png', NULL, 1, 1, '2017-03-10 19:00:00'),
(31, 9, '2017-03-15 18:40:14', 1, '1_1489603214_bg.jpg', NULL, 1, 1, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `course_tbl`
--

CREATE TABLE IF NOT EXISTS `course_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'unique id',
  `title` varchar(255) DEFAULT NULL,
  `desc` varchar(255) DEFAULT NULL,
  `added_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(1) DEFAULT '1' COMMENT '1=active,2=delete,0=deactive',
  `faculty_id` int(11) DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `course_tbl`
--

INSERT INTO `course_tbl` (`id`, `title`, `desc`, `added_date`, `status`, `faculty_id`, `department_id`, `code`, `created_by`) VALUES
(3, 'Object oriented programming', 'this is object oriented programming', '2017-03-15 19:09:37', 1, NULL, 2, 'oop', 1),
(4, 'Databases', 'Collection of Data', '2017-03-15 19:09:13', 1, NULL, 2, 'db', 15),
(5, 'Communication skills', 'This is about english', '2017-03-15 19:08:49', 1, NULL, 3, 'english', 1),
(6, 'accounting', 'this is all about accounting', '2017-03-15 16:41:43', 1, NULL, 3, 'account', 1);

-- --------------------------------------------------------

--
-- Table structure for table `department_tbl`
--

CREATE TABLE IF NOT EXISTS `department_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'unique id',
  `title` varchar(255) DEFAULT NULL,
  `desc` varchar(255) DEFAULT NULL,
  `added_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(1) DEFAULT '1' COMMENT '1=active,2=delete,0=deactive',
  `faculty_id` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `department_tbl`
--

INSERT INTO `department_tbl` (`id`, `title`, `desc`, `added_date`, `status`, `faculty_id`, `created_by`) VALUES
(2, 'BSCS', 'Computer Science fields is going to be good in coming era.', '2017-03-15 18:42:05', 1, 2, 1),
(3, 'BBA', 'This is Business administration department, and some subject are about finance and accountin.g', '2017-01-08 05:48:01', 1, 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `faculty_tbl`
--

CREATE TABLE IF NOT EXISTS `faculty_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'unique id',
  `title` varchar(255) DEFAULT NULL,
  `desc` varchar(255) DEFAULT NULL,
  `added_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(1) DEFAULT '1' COMMENT '1=active,2=delete,0=deactive',
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `faculty_tbl`
--

INSERT INTO `faculty_tbl` (`id`, `title`, `desc`, `added_date`, `status`, `created_by`) VALUES
(2, 'Faculty1', 'This is faculty1', '2017-01-07 17:45:33', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `teacher_assistant_tbl`
--

CREATE TABLE IF NOT EXISTS `teacher_assistant_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `teacher_id` int(11) NOT NULL,
  `create_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` int(1) DEFAULT '1' COMMENT '1=active,2=delete,0=deactive',
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `teacher_course_tbl`
--

CREATE TABLE IF NOT EXISTS `teacher_course_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `teacher_id` int(11) NOT NULL,
  `department_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `create_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` int(1) DEFAULT '1' COMMENT '1=active,2=delete,0=deactive',
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `teacher_course_tbl`
--

INSERT INTO `teacher_course_tbl` (`id`, `teacher_id`, `department_id`, `course_id`, `create_date`, `status`, `created_by`) VALUES
(4, 15, 2, 3, '2017-01-14 16:48:38', 1, 1),
(5, 15, 2, 4, '2017-01-14 16:48:39', 1, 15),
(6, 16, 3, 5, '2017-01-14 20:50:26', 1, 1),
(7, 15, 3, 5, '2017-02-18 04:44:10', 1, 15),
(8, 15, 3, 6, '2017-03-15 16:42:57', 1, 1),
(9, 16, 3, 6, '2017-03-15 18:40:41', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_tbl`
--

CREATE TABLE IF NOT EXISTS `user_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'unique id',
  `title` varchar(255) DEFAULT NULL,
  `desc` varchar(255) DEFAULT NULL,
  `added_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(1) DEFAULT '1' COMMENT '1=active,2=delete,0=deactive',
  `email` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `qualification` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `type` int(2) DEFAULT NULL COMMENT '0=superadmin,1=admin,2=teacher,3=assistant',
  `address` varchar(255) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `approved_by` int(11) DEFAULT NULL,
  `assistant_id` int(11) DEFAULT NULL COMMENT 'Assistant',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

--
-- Dumping data for table `user_tbl`
--

INSERT INTO `user_tbl` (`id`, `title`, `desc`, `added_date`, `status`, `email`, `phone`, `qualification`, `password`, `type`, `address`, `created_by`, `approved_by`, `assistant_id`) VALUES
(1, 'rahil', 'i am rahil', '2017-01-20 20:23:53', 1, 'rahilcomsian@gmail.com', '03345227139', 'bscs', 'YWRtaW5fLV9zZWN1cmU=', 0, 'isb', 1, 1, NULL),
(2, 'admin', 'admin', '2017-01-20 20:24:04', 1, 'admin@gmail.com', '03345227139', 'bscs', 'YWRtaW5fLV9zZWN1cmU=', 1, 'isb', 1, 1, NULL),
(15, 'zara', NULL, '2017-02-18 16:18:44', 1, 'zara@gmail.com', '090078601', 'bs', 'emFyYV8tX3NlY3VyZQ==', 2, 'house 143', 1, 1, NULL),
(16, 'hira', NULL, '2017-03-08 18:07:38', 1, 'hira@gmail.com', '09566666', 'ms', 'aGlyYV8tX3NlY3VyZQ==', 2, 'house 10', 1, 1, 23),
(23, 'tahir', NULL, '2017-02-18 04:08:33', 1, 'tahir@gmail.com', '09888', 'tyu', 'dGFoaXJfLV9zZWN1cmU=', 3, 'ytyutuy', 1, NULL, NULL),
(24, 'Abrar Khalid', NULL, '2017-03-13 09:13:32', 1, 'ali.ahmer5@hotmail.com', '123456', 'Phd', 'MTIzNDU2NzhfLV9zZWN1cmU=', 2, 'wdfg', 2, NULL, NULL);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
