
<!-- <div class="extra">
  <div class="extra-inner">
    <div class="container">
      <div class="row">
                    <div class="span3">
                        <h4>
                            Admin Template</h4>
                        <ul>
                            <li><a href="javascript:;">EGrappler.com</a></li>
                            <li><a href="javascript:;">Web Development Resources</a></li>
                            <li><a href="javascript:;">Responsive HTML5 Portfolio Templates</a></li>
                            <li><a href="javascript:;">Free Resources and Scripts</a></li>
                        </ul>
                    </div>
                
                    <div class="span3">
                        <h4>
                            Support</h4>
                        <ul>
                            <li><a href="javascript:;">Frequently Asked Questions</a></li>
                            <li><a href="javascript:;">Ask a Question</a></li>
                            <li><a href="javascript:;">Video Tutorial</a></li>
                            <li><a href="javascript:;">Feedback</a></li>
                        </ul>
                    </div>
                    
                    <div class="span3">
                        <h4>
                            Something Legal</h4>
                        <ul>
                            <li><a href="javascript:;">Read License</a></li>
                            <li><a href="javascript:;">Terms of Use</a></li>
                            <li><a href="javascript:;">Privacy Policy</a></li>
                        </ul>
                    </div>
                    
                    <div class="span3">
                        <h4>
                            Open Source jQuery Plugins</h4>
                        <ul>
                            <li><a href="javascript:;">Open Source jQuery Plugins</a></li>
                            <li><a href="javascript:;">HTML5 Responsive Tempaltes</a></li>
                            <li><a href="javascript:;">Free Contact Form Plugin</a></li>
                            <li><a href="javascript:;">Flat UI PSD</a></li>
                        </ul>
                    </div>
                   
                </div>
     
    </div>
     
  </div>
  
</div> -->
<!-- /extra -->

<div class="footer">
  <div class="footer-inner">
    <div class="container">
      <div class="row">
        <div class="span12"> Copyright &copy; <?=date('Y');?>.&nbsp;<?=COPY_RIGHT;?>
        <!--<a href="index"><?=PROJECT_NAME;?></a> </div>-->
        <!-- /span12 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /footer-inner --> 
</div>
<!-- /footer --> 
<!-- Le javascript 
================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="js/jquery-1.7.2.min.js"></script> 
<script src="js/excanvas.min.js"></script> 
<!--<script src="js/chart.min.js" type="text/javascript"></script> -->
<script src="js/bootstrap.js"></script>
<!--<script language="javascript" type="text/javascript" src="js/full-calendar/fullcalendar.min.js"></script>-->
 
<script src="js/base.js"></script> 

<script src="js/signin.js"></script>
<script src="js/custom_js.js"></script>
<script src="js/bootstrap-filestyle.min.js"></script>

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<!--<script src="js/guidely/guidely.min.js"></script>-->
<style>.main > .main-inner { min-height:518px !important;}</style>
<? include_once("modal.php"); ?> 
</div>
</body>
</html>  
