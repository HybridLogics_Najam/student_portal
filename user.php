<? include_once("head.php"); ?>
<?

/*types
faculty=1
department =2 
course=3*/  
if(@$type=='') { ?><center><b><? echo "Invalid access"; ?></b></center><? }
else {  
$ignore = array('module','action','proceed','back_url','id','approved_by');

if($type=='2') 
{    
if(@$fid=='') {/* ?><center><b><? die('Invalid access'); ?></b></center><?  */} 
    $module = "teacher"; 
	$tbl = "user_tbl";  
	$heading = array('Name','Email','Phone','Residence','Qualification','Courses','Assistant');  
	$col = array('title','email','phone','address','qualification','total_courses','assistant_id','id','approved_by');
    $main_heading = "Teacher";
    $sub_heading = @$_SESSION['admin']?"Teacher list":"Details";
	if(@$_SESSION['admin']){$heading[] = 'Active Status';$col[] = 'status';}
	if(@$fid!=''){$condition.=" AND faculty_id= ".$fid; }  
	$records = RECORD_PER_PAGE;
	if(@$_SESSION['current_user_type']=='2'){$teacher_cond = " AND u.id = ".$_SESSION['current_user_id']." AND status=1 ";} else{$teacher_cond="";}  
	$query = "SELECT u.*,
              (select count(tc.id) from teacher_course_tbl tc where tc.teacher_id = u.id) as \"total_courses\"
			  FROM user_tbl u   
			  WHERE u.type=2 AND status!=2
			  ".@$teacher_cond."
			  GROUP BY u.id 
			  ORDER BY u.id DESC";
	$data = $myObj->doPagination_query('',$query,$records,'');   
	  	
}  

//echo "<pre>";print_r($data); exit;
?>  
 
<div class="main"> 
  <div class="main-inner">
    <div class="container">
      <div class="row">
        
        <!-- /span6 -->
        <div class="span12">
          
         
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3><?=$sub_heading;?></h3>  
               <? if(@$_SESSION['admin']){?>
               <a href="javascript:;" class=""  onclick="modal_data('add','<?=$module;?>','c','','<?=base64_encode($_SERVER['REQUEST_URI']);?>');"><i class="btn-icon-only icon-plus" style="font-size: 37px;  
    float: right;
    margin: 2px 0px 0px 0px;"></i></a>    <? } ?> 		  
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <?  if(sizeof($data)>0) { ?> 
              <table class="table table-striped table-bordered">
                <thead>
                  <tr>
                  <th width="10">S.No</th>
				  <?
				  
				   foreach($heading as $head){?><th><?=$head;?></th><? } ?> 
                     
                    <th class="td-actions"> </th>
                  </tr>
                </thead>
                <tbody>
                  <? 
				 
				  foreach($data as $key_data_=>$data_){ ?>   
                  <tr id="tr_<?=$key_data_+1;?>">
                  <td><?=$key_data_+1;?></td>
                    <?  
						 foreach($col as $column){ 
							 if ( !(in_array($column,$ignore)) ){
						       ?><td><? if($column=="total_courses"){
								   if(@$data_->$column>0){
								   ?>
								   <a href="subject?type=4&tcid=<?=$data_->id;?>"><?=$data_->$column;?></a>
								   <?
								   } else {echo "0";}
							   }


                 else if($column=="assistant_id"){
                   if(@$data_->$column>0){
                     $assistant_data = $myObj->getData("","user_tbl",array('title')," id=".$data_->$column);
					 
					 echo @$assistant_data[0]->title;
                   } else {?> 
				   <a href="javascript:;" onclick=" modal_data('add','assistant','c','_<?=$data_->id;?>','<?=base64_encode($_SERVER['REQUEST_URI']);?>');">Add</a>
				   <? }
                 }

							    else if($column=="status"){
								   if(@$data_->$column=='0' && @$data_->approved_by==''){?><font style="color:orange;">
                                   <a href="javascript:;" onclick="var app = confirm('are you sure you want to approve');if(app){ modal_data('add','approve','ap','<?=$data_->id;?>','<?=base64_encode($_SERVER['REQUEST_URI']);?>');}">Approve</a><font style="font-size:10px;">&nbsp;&nbsp;(Approval Pending)</font>
                                   </font><? } 
								   else if(@$data_->$column=='2'){?><font style="color:red;">Deactive</font><? }
								   else if(@$data_->$column=='1'){ ?><font style="color:green;">Active</font><? }   
							   }
							   
							    
							   else {echo $data_->$column;}
								   ?></td><?    
						       }
						 } ?>
                 
                    <td class="td-actions">  
                       
                    <a href="javascript:;" class="btn btn-small btn-success" onclick="modal_data('add','<?=$module;?>','u','<?=$data_->id;?>','<?=base64_encode($_SERVER['REQUEST_URI']);?>');"><i class="btn-icon-only icon-edit"> </i></a>  
					<? if(@$_SESSION['admin']) {?>
                    <a href="javascript:;" class="btn btn-danger btn-small"  onclick="var del = confirm('are you sure you want to delete');if(del){modal_data('add','<?=$module;?>','d','<?=$data_->id;?>','<?=base64_encode($_SERVER['REQUEST_URI']);?>');$('#tr<?=$key_data_+1;?>').remove();
             $('#tr_<?=$key_data_+1;?>').hide('slow',function(){ $('#tr_<?=$key_data_+1;?>').remove(); });       
                    }"><i class="btn-icon-only icon-remove"> </i></a></td>    
                    <? } ?> 
                  </tr> 
                <? }  ?> 
                </tbody>
              </table> 
              </div>
			  <div class="paging" style="float:right;">
			  <? $myObj->doPagination_query('',$query,$records,'1'); ?> 
			  </div>
              <? } else { ?>
					<center><b>No Data Found</b></center>     
					<? } ?>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
          
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->
<? } ?>
<? include_once("tail.php"); ?>